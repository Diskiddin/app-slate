//
//  Attendance.swift
//  Slate
//
//  Created by Eric Scaramuzzo on 12/15/17.
//  Copyright © 2017 Eric Scaramuzzo. All rights reserved.
//

import Foundation

class Attendance: Hashable {
    
    public var dateTaken:Date!
    public var attendance : [String:Bool] = [:]
    
    var hashValue: Int {
        return dateTaken.hashValue
    }
    
    static func ==(lhs: Attendance, rhs: Attendance) -> Bool {
        return lhs.dateTaken == rhs.dateTaken
    }
}
