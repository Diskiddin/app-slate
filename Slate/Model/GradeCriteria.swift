//
//  gradeCriteria.swift
//  Slate
//
//  Created by Eric Scaramuzzo on 12/8/17.
//  Copyright © 2017 Eric Scaramuzzo. All rights reserved.
//

import Foundation
class GradeCriteria: Equatable {
    static func ==(lhs: GradeCriteria, rhs: GradeCriteria) -> Bool {
        return lhs.name == rhs.name
    }
    
    public var lowerBound:Double!
    public var upperBound:Double!
    public var name:String!
    
    init(lowerBound:Double, upperBound:Double, name:String) {
        self.lowerBound = lowerBound
        self.upperBound = upperBound
        self.name = name
    }
    
    init(lowerBound:Double) {
        self.lowerBound = lowerBound
        self.upperBound = Double.greatestFiniteMagnitude
        self.name = "A+"
    }
    
    
}
