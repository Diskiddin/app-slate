//
//  Category.swift
//  Slate
//
//  Created by Eric Scaramuzzo on 11/30/17.
//  Copyright © 2017 Eric Scaramuzzo. All rights reserved.
//

import Foundation

class Category: Hashable{
    private(set) public var name: String!
    public var weight: Double!
    
    var hashValue: Int {
        return name.hashValue + weight.hashValue
    }
    
    init(name: String, weight: Double) {
        self.name = name
        self.weight = weight
    }
    
    static func ==(lhs: Category, rhs: Category) -> Bool {
        return lhs.name == rhs.name
    }
}
