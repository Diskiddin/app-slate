//
//  Student.swift
//  Slate
//
//  Created by Eric Scaramuzzo on 11/29/17.
//  Copyright © 2017 Eric Scaramuzzo. All rights reserved.
//

import Foundation

class Student: Hashable {
    
    var hashValue: Int {
        return id.hashValue
    }
    
    static func ==(lhs: Student, rhs: Student) -> Bool {
        return (lhs.name == rhs.name) && (lhs.id == rhs.id)
    }
    
    private(set) public var name: String!
    private(set) public var id: String!
    public var totalGrade: Double!
    public var grades: [Assignment:Double] = [:]
    
    init(forName name: String, forID id: String) {
        self.name = name
        self.id = id
        totalGrade = 100.00
    }
    
    func setName(name : String ) {
        self.name = name
    }
    
    func addAssignment(assignment : Assignment) {
        grades[assignment] = assignment.possiblePoints
    }
    
    func getAssignmentGrade(assignment : Assignment) -> Double {
        return grades[assignment]!
    }
}
