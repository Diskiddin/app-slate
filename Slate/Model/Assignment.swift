//
//  Assignment.swift
//  Slate
//
//  Created by Eric Scaramuzzo on 11/30/17.
//  Copyright © 2017 Eric Scaramuzzo. All rights reserved.
//

import Foundation

class Assignment: Hashable {
    
    private(set) public var name: String!
    private(set) public var category: Category
    private(set) public var possiblePoints: Double!
    private(set) public var description: String!
    
    init(forName name: String, forCategory category: Category, forPossiblePoints possiblePoints: Double, forDescription  description: String) {
        self.name = name
        self.category = category
        self.possiblePoints = possiblePoints
        self.description = description
    }
    
    func setName(name: String) {
        self.name = name
    }
    
    func setDescription(description : String) {
        self.description = description
    }
    
    func setCategory(category: Category) {
        self.category = category
    }
    
    func getCategory() -> Category {
        return category
    }
    
    func setPossiblePoints(points : Double) {
        self.possiblePoints = points
    }
    
    var hashValue: Int {
        return ObjectIdentifier(self).hashValue
    }
    
    static func ==(lhs: Assignment, rhs: Assignment) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }
}
