//
//  Course.swift
//  Slate
//
//  Created by Eric Scaramuzzo on 11/28/17.
//  Copyright © 2017 Eric Scaramuzzo. All rights reserved.
//

import Foundation
import CoreData

class Course: Equatable {
    static func ==(lhs: Course, rhs: Course) -> Bool {
        return (lhs.name == rhs.name) && (lhs.id == rhs.id)
    }
    
    
    private var name : String!
    private var id : String!
    public var students: [Student] = []
    public var assignments: [Assignment] = []
    public var categories: [Category] = []
    public var unassignedCategory: Category = Category(name: "Unassigned", weight: 0.0)
    public var gradeCriterias:[GradeCriteria] = []
    public var criteriaHasBeenChanged = false
    public var attendances:[Attendance] = []
    
    
    init(forName name : String, forId id: String) {
        self.name = name
        self.id = id
        gradeCriterias.append(GradeCriteria(lowerBound: 97, upperBound: 100.00, name: "A+"))
        gradeCriterias.append(GradeCriteria(lowerBound: 93, upperBound: 96.99, name: "A"))
        gradeCriterias.append(GradeCriteria(lowerBound: 90, upperBound: 92.99, name: "A-"))
        gradeCriterias.append(GradeCriteria(lowerBound: 87, upperBound: 89.99, name: "B+"))
        gradeCriterias.append(GradeCriteria(lowerBound: 83, upperBound: 86.99, name: "B"))
        gradeCriterias.append(GradeCriteria(lowerBound: 80, upperBound: 82.99, name: "B-"))
        gradeCriterias.append(GradeCriteria(lowerBound: 77, upperBound: 79.99, name: "C+"))
        gradeCriterias.append(GradeCriteria(lowerBound: 73, upperBound: 76.99, name: "C"))
        gradeCriterias.append(GradeCriteria(lowerBound: 70, upperBound: 72.99, name: "C-"))
        gradeCriterias.append(GradeCriteria(lowerBound: 67, upperBound: 69.99, name: "D+"))
        gradeCriterias.append(GradeCriteria(lowerBound: 63, upperBound: 66.99, name: "D"))
        gradeCriterias.append(GradeCriteria(lowerBound: 60, upperBound: 62.99, name: "D-"))
        gradeCriterias.append(GradeCriteria(lowerBound: 0, upperBound: 59.99, name: "F"))
    }
    
    func setName(name : String) {
        self.name = name
    }
    
    func getName() -> String {
        return name
    }
    
    func getId() -> String {
        return id
    }
    
    func getLetterGrade(student:Student) -> String {
        var selectedGc:GradeCriteria!
        let tempStringGrade:String = String(format:"%.2f", student.totalGrade)
        let concatenatedGrade:Double = Double(tempStringGrade)!
        for gc in gradeCriterias {
            if concatenatedGrade <= gc.upperBound && concatenatedGrade >= gc.lowerBound {
                selectedGc = gc
            }
        }
        if selectedGc == nil {
            selectedGc = gradeCriterias[0]
        }
        return selectedGc.name
    }
    
    func getAssignmentNames() -> [String] {
        var names : [String] = []
        for a in assignments {
            names.append(a.name)
        }
        return names
    }
    
    func removeAssignment(assignment: Assignment) {
        let index = assignments.index(of: assignment)
        assignments.remove(at: index!)
        for s in students {
            let index = s.grades.index(forKey: assignment)
            s.grades.remove(at: index!)
        }
    }
    
    func getAssignment(assignment : Assignment) -> Assignment {
        let index = assignments.index(of: assignment)
        return assignments[index!]
    }
    
    func addCategory(category: Category) {
        categories.append(category)
    }
    
    func getCategoryIds() -> [String] {
        var ids : [String] = []
        for c in categories {
            ids.append(c.name)
        }
        return ids
    }
    
    func addStudent(student : Student) {
        for a in assignments {
            student.addAssignment(assignment: a)
        }
        students.append(student)
    }
    
    func addAssignmentAtFromt(assignment : Assignment) {
        assignments.insert(assignment, at: 0)
        for s in students {
            s.addAssignment(assignment: assignment)
        }
    }
    
    func addAssignment(assignment : Assignment) {
        assignments.append(assignment)
        for s in students {
            s.addAssignment(assignment: assignment)
        }
    }
    
    func generateStatistics() -> [String:Double] {
        var statistics : [String:Double] = [:]
        
        if students.count == 0 || assignments.count == 0 || categories.count == 0{
            statistics["Mean"] = 100
            statistics["Min"] = 100
            statistics["Max"] = 100
            return statistics
        } else {
            
            var totalGrades:Double = 0
            for s in students {
                totalGrades += s.totalGrade
            }
            let mean = totalGrades/Double(students.count)
            
            var minGrade = students[0].totalGrade!
            for s in students {
                if s.totalGrade < minGrade {
                    minGrade = s.totalGrade
                }
            }
            
            var maxGrade = students[0].totalGrade!
            for s in students {
                if s.totalGrade > maxGrade {
                    maxGrade = s.totalGrade
                }
            }
            
            statistics["Mean"] = mean
            statistics["Min"] = minGrade
            statistics["Max"] = maxGrade
            
            return statistics
        }
        
        
    }
    
    func generateStatisticsForAssignment(assignment: Assignment) -> [String:Double]{
        var statistics : [String:Double] = [:]
        if students.count == 0 {
            statistics["Mean"] = 100
            statistics["Min"] = 100
            statistics["Max"] = 100
        } else {
            var totalGrades:Double = 0
            for s in students {
                totalGrades += s.grades[assignment]!
            }
            let mean = totalGrades/Double(students.count)
            
            var minGrade = students[0].grades[assignment]
            for s in students {
                if s.grades[assignment]! < minGrade! {
                    minGrade = s.grades[assignment];
                }
            }
            
            var maxGrade = students[0].grades[assignment]
            for s in students {
                if s.grades[assignment]! > maxGrade! {
                    maxGrade = s.grades[assignment]
                }
            }
            
            statistics["Mean"] = mean
            statistics["Min"] = minGrade
            statistics["Max"] = maxGrade
        }
        return statistics
    }
    
    func calculateGrade(student: Student) -> Double{
        if student.grades.count > 0 {
            var grade:Double = 0
            var categoryWeights:[Double] = []
            var categoryGrades:[Double] = []
            var possibleCategoryGrades:[Double] = []
            if categories.count == 0 {
                return 100
            }
            for index in 0...categories.count - 1 {
                categoryGrades.append(0)
                possibleCategoryGrades.append(0)
                var categoryHasAssignment = false
                for a in student.grades.keys {
                    if a.getCategory() == categories[index] {
                        categoryGrades[index] += student.getAssignmentGrade(assignment: a)
                        possibleCategoryGrades[index] += a.possiblePoints
                        categoryHasAssignment = true
                    }
                }
                if categoryHasAssignment {
                    categoryWeights.append(categories[index].weight)
                } else {
                    categoryWeights.append(0)
                }
            }
            var totalCategoryWeights:Double = 0
            for weight in categoryWeights {
                totalCategoryWeights += weight
            }
            var actualCategoryGrades:[Double] = []
            for index in 0...categoryGrades.count - 1{
                actualCategoryGrades.append(0)
                if possibleCategoryGrades[index] != 0 {
                    actualCategoryGrades[index] = categoryGrades[index] / possibleCategoryGrades[index]
                } else if possibleCategoryGrades[index] == 0 && categoryWeights[index] != 0{
                    actualCategoryGrades[index] = (categoryGrades[index] / categoryWeights[index]) + 1
                } else {
                    actualCategoryGrades[index] = 0
                }
            }
            var categoryWeightGrades:[Double] = []
            for index in 0...actualCategoryGrades.count  - 1{
                categoryWeightGrades.append(0)
                categoryWeightGrades[index] = actualCategoryGrades[index] * categoryWeights[index]
            }
            var totalWeights:Double = 0
            var totalWeightGrades:Double = 0
            for index in 0...categoryWeightGrades.count  - 1{
                
                totalWeights += categoryWeights[index]
                totalWeightGrades += categoryWeightGrades[index]
            }
            grade = totalWeightGrades / totalWeights
            student.totalGrade = grade * 100
            return grade * 100
            
        } else {
            student.totalGrade = 100
            return 100
        }
    }
}

