//
//  DataService.swift
//  Slate
//
//  Created by Eric Scaramuzzo on 11/28/17.
//  Copyright © 2017 Eric Scaramuzzo. All rights reserved.
//

import Foundation
import CoreData

class DataService {
    static let instance = DataService()
    public var courses : [Course] = []//[Course(forName: "Course 1", forId: "1234"), Course(forName: "Course 2", forId: "12345")]
    public var selectedCourse: Course?
    public var selectedCategory: Category?
    public var selectedAssignment: Assignment?
    public var selectedStudent: Student?
    public var changesMade:Bool = false
    
    init() {
    }
    
    func getId() -> [String] {
        var ids : [String] = []
        for c in courses {
            ids.append(c.getId())
        }
        return ids
    }
    
    func addFiftyStudents() {
        for i in 4...50 {
            courses[0].addStudent(student: Student(forName: "Name", forID: String(i)))
        }
    }
    
    func save () {
        if(changesMade) {
            changesMade = false
            
            let container = appDelegate?.persistentContainer
            container?.performBackgroundTask({ (context) in
                let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Data")
                do {
                    let _ = try context.execute(NSBatchDeleteRequest(fetchRequest: fetchRequest))
                } catch {
                    
                }
                
                let data = Data(context: context)
                
                var courseArray : [CourseData] = []
                
                for c in self.courses {
                    
                    let courseData = CourseData(context: context)
                    courseData.name = c.getName()
                    courseData.id = c.getId()
                    var categories : [CategoryData] = []
                    for c in c.categories {
                        let categoryData = CategoryData(context: context)
                        categoryData.name = c.name
                        categoryData.weight = c.weight
                        categories.append(categoryData)
                    }
                    var students : [StudentData] = []
                    for s in c.students {
                        let studentData = StudentData(context: context)
                        studentData.name = s.name
                        studentData.id = s.id
                        var grades : [GradeData] = []
                        _ = c.calculateGrade(student: s)
                        studentData.totalGrade = s.totalGrade
                        for a in s.grades {
                            let newGrade = GradeData(context: context)
                            newGrade.assignmentName = a.key.name
                            newGrade.grade = a.value
                            grades.append(newGrade)
                        }
                        studentData.grades = NSSet(array: grades)
                        students.append(studentData)
                    }
                    var assignments : [AssignmentData] = []
                    for a in c.assignments {
                        let assignmentData = AssignmentData(context: context)
                        assignmentData.name = a.name
                        assignmentData.descriptionString = a.description
                        assignmentData.possiblePoints = a.possiblePoints
                        let categoryData = CategoryData(context: context)
                        categoryData.name = a.category.name
                        categoryData.weight = a.category.weight
                        assignmentData.category = categoryData
                        assignments.append(assignmentData)
                    }
                    var criterias : [GradeCriteriaData] = []
                    if c.criteriaHasBeenChanged {
                        for criteria in c.gradeCriterias {
                            let criteriaData = GradeCriteriaData(context: context)
                            criteriaData.lowerBound = criteria.lowerBound
                            criteriaData.upperBound = criteria.upperBound
                            criteriaData.name = criteria.name
                            criterias.append(criteriaData)
                        }
                    }
                    var attendances: [AttendanceData] = []
                    for a in c.attendances {
                        let attendanceData = AttendanceData(context: context)
                        attendanceData.dateTaken = a.dateTaken
                        var singleAttendances : [SingleAttendanceData] = []
                        for key in a.attendance.keys {
                            let singleAttendanceData = SingleAttendanceData(context: context)
                            singleAttendanceData.id = key
                            singleAttendanceData.value = a.attendance[key]!
                            singleAttendances.append(singleAttendanceData)
                        }
                        attendanceData.attendances = NSSet(array: singleAttendances)
                        attendances.append(attendanceData)
                    }
                    
                    courseData.attendance = NSSet(array: attendances)
                    courseData.criterias = NSSet(array: criterias)
                    courseData.assignments = NSSet(array: assignments)
                    courseData.students = NSSet(array: students)
                    courseData.categories = NSSet(array: categories)
                    courseArray.append(courseData)
                }
                data.courses = NSSet(array: courseArray)
                do {
                    try context.save()
                    print("Saved")
                } catch {
                    self.changesMade = true
                    debugPrint("Could not save: \(error.localizedDescription)")
                }
            })
            
        } else {
            print("Don't Save")
        }
    }
}
