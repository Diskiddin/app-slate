//
//  CourseCell.swift
//  Slate
//
//  Created by Eric Scaramuzzo on 11/28/17.
//  Copyright © 2017 Eric Scaramuzzo. All rights reserved.
//

import UIKit

class CourseCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var mainView: UIView!
    
    var course : Course!
    
    func updateView() {
        titleLabel.text = course.getName()
        subtitleLabel.text = "Id: " + course.getId()
    }
    
}
