//
//  AssignmentStudentCell.swift
//  Slate
//
//  Created by Eric Scaramuzzo on 12/4/17.
//  Copyright © 2017 Eric Scaramuzzo. All rights reserved.
//

import UIKit

protocol changeAssignmentTextField {
    func changeTextField(assignment: Assignment)
}

class AssignmentStudentCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var assignmentNameLabel: UILabel!
    @IBOutlet weak var assignmentCategoryLabel: UILabel!
    @IBOutlet weak var assignmentGradeInput: UITextField!
    @IBOutlet weak var assignmentTotalGradeLabel: UILabel!
    
    
    var assignment:Assignment!
    var protocolDelegate: changeAssignmentTextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        assignmentGradeInput.delegate = self
    }
    
    func updateViews(name: String, category: String, grade: Double, totalGrade: Double) {
        assignmentNameLabel.text = name
        assignmentCategoryLabel.text = category
        assignmentGradeInput.text = String(format: "%.2f", grade)
        assignmentTotalGradeLabel.text = String(format: "/ %.2f", totalGrade)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        protocolDelegate.changeTextField(assignment: assignment)
        return false
    }
    
}
