//
//  StudentAssignmentCellTableViewCell.swift
//  Slate
//
//  Created by Eric Scaramuzzo on 12/3/17.
//  Copyright © 2017 Eric Scaramuzzo. All rights reserved.
//

import UIKit

protocol changeTextFieldProtocol {
    func changeTextField(student: Student)
}

class StudentAssignmentCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var studentNameLabel: UILabel!
    @IBOutlet weak var studentIdLabel: UILabel!
    @IBOutlet weak var gradeTextInput: UITextField!
    
    public var student:Student!
    
    public var protocolDelegate:changeTextFieldProtocol!
    
    public var cellNumber:Int!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        gradeTextInput.delegate = self
    }
    
    func updateView() {
        studentNameLabel.text = student.name
        studentIdLabel.text = "Id: " + student.id
        gradeTextInput.text = String(format: "%.2f", student.grades[DataService.instance.selectedAssignment!]!)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        protocolDelegate.changeTextField(student: student)
        return false
    }

}
