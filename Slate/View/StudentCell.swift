//
//  StudentCell.swift
//  Slate
//
//  Created by Eric Scaramuzzo on 11/29/17.
//  Copyright © 2017 Eric Scaramuzzo. All rights reserved.
//

import UIKit

class StudentCell: UITableViewCell {

    
    @IBOutlet weak var gradeLabel: UILabel!
    @IBOutlet var letterGradeLabel: UILabel!
    @IBOutlet weak var studentIdLabel: UILabel!
    @IBOutlet weak var studentNameLabel: UILabel!
    var student : Student!
    
    func updateViews() {
        gradeLabel.text = String(format: "%.2f%%", (DataService.instance.selectedCourse?.calculateGrade(student: student))!)
        studentNameLabel.text = student.name
        studentIdLabel.text = "Id: " + student.id
        letterGradeLabel.text = DataService.instance.selectedCourse?.getLetterGrade(student: student)
    }

}
