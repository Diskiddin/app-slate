//
//  ShadorLabel.swift
//  Slate
//
//  Created by Eric Scaramuzzo on 12/6/17.
//  Copyright © 2017 Eric Scaramuzzo. All rights reserved.
//

import UIKit

class ShadowLabel: UILabel {

    override func awakeFromNib() {
        updateView()
    }
    
    func updateView(){
        layer.backgroundColor = UIColor.white.cgColor
        layer.shadowOffset = CGSize(width: 4, height: 4)
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowRadius = 5
        layer.shadowOpacity = 0.5
        layer.masksToBounds = false
    }

}
