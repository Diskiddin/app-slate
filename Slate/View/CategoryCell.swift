//
//  CategoryCell.swift
//  Slate
//
//  Created by Eric Scaramuzzo on 12/5/17.
//  Copyright © 2017 Eric Scaramuzzo. All rights reserved.
//

import UIKit

protocol changeCategoryWeightProtocol {
    func changeCategoryWeight(category: Category)
}

class CategoryCell: UITableViewCell, UITextFieldDelegate {

    
    @IBOutlet var categoryNameTextField: UITextField!
    @IBOutlet var categoryWeightTextField: UITextField!
    
    var category:Category!
    var protocolDelegate:changeCategoryWeightProtocol!
    
    func updateView() {
        categoryNameTextField.text = category.name
        categoryWeightTextField.text = String(format: "%.2f", category.weight)
        
        categoryWeightTextField.delegate = self
        
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == categoryWeightTextField{
            protocolDelegate.changeCategoryWeight(category: category)
            return false
        } else if textField == categoryNameTextField {
            return false
        } else {
            return false
        }
    }
    
}
