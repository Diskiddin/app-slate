//
//  BorderButtonView.swift
//  Slate
//
//  Created by Eric Scaramuzzo on 12/13/17.
//  Copyright © 2017 Eric Scaramuzzo. All rights reserved.
//

import UIKit

class BorderButtonView: UIButton {

    func updateView(){
        layer.cornerRadius = 3.0
        layer.borderWidth = 1.0
        layer.borderColor = #colorLiteral(red: 0.937254902, green: 0.5568627451, blue: 0.4823529412, alpha: 1)
        
        setTitleColor(#colorLiteral(red: 0.937254902, green: 0.5568627451, blue: 0.4823529412, alpha: 1), for: .normal)
        setTitleColor(#colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1), for: .disabled)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        updateView()
    }

}
