//
//  AssignmentCell.swift
//  Slate
//
//  Created by Eric Scaramuzzo on 11/30/17.
//  Copyright © 2017 Eric Scaramuzzo. All rights reserved.
//
import UIKit

class AssignmentCell: UITableViewCell {

    
    @IBOutlet var assignmentNameLabel: UILabel!
    @IBOutlet var assignmentCategoryLabel: UILabel!
    
    var assignment : Assignment!
    
    func updateViews() {
        assignmentNameLabel.text = assignment.name
        assignmentCategoryLabel.text = assignment.category.name
    }
    
    
    
}
