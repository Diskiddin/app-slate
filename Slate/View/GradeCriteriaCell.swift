//
//  GradeCriteriaCell.swift
//  Slate
//
//  Created by Eric Scaramuzzo on 12/8/17.
//  Copyright © 2017 Eric Scaramuzzo. All rights reserved.
//

import UIKit

protocol updateCriteria {
    func updateCriteria(gradeCriteria: GradeCriteria)
}

class GradeCriteriaCell: UITableViewCell, UITextFieldDelegate {
    
    @IBOutlet var lowerBoundLabel: UILabel!
    @IBOutlet var upperBoundTextField: UITextField!
    @IBOutlet var letterGradeLabel: UILabel!
    
    var gradeCriteria:GradeCriteria!
    var criteriaDelegate:updateCriteria!
    var oldValue:String!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        upperBoundTextField.delegate = self
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == upperBoundTextField{
            if let index = textField.text?.index(of: "."){
                if string == "." {
                    return false
                }
                if range.location > index.encodedOffset + 2 {
                    return false
                }
            } else {
                if range.location + string.count > 3 && string != "."{
                    return false
                }
                return true
            }
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        oldValue = textField.text
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        if textField == upperBoundTextField {
            if textField.text == "" {
                textField.text = oldValue
                return
            }
            if let newBound = Double(String(format: "%.2f", Double(textField.text!)!)) {
                gradeCriteria.upperBound = newBound
                criteriaDelegate.updateCriteria(gradeCriteria: gradeCriteria)
            }
        }
    }
    
    func updateView(){
        lowerBoundLabel.text = String(format: "%.2f", gradeCriteria.lowerBound)
        if gradeCriteria.upperBound == Double.greatestFiniteMagnitude {
            upperBoundTextField.text = "100.00"
        } else {
            upperBoundTextField.text = String(format: "%.2f", gradeCriteria.upperBound)
        }
        letterGradeLabel.text = gradeCriteria.name
    }
}

