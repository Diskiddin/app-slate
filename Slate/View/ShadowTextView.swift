//
//  ShadowTextView.swift
//  Slate
//
//  Created by Eric Scaramuzzo on 12/3/17.
//  Copyright © 2017 Eric Scaramuzzo. All rights reserved.
//

import UIKit

class ShadowTextView: UITextView {

    func updateView() {
        clipsToBounds = false
        layer.shadowOpacity = 0.2
        layer.shadowOffset = CGSize(width: 2, height: 2)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        updateView()
    }
    
}
