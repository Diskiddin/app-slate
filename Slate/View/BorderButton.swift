//
//  BorderButton.swift
//  Slate
//
//  Created by Eric Scaramuzzo on 11/29/17.
//  Copyright © 2017 Eric Scaramuzzo. All rights reserved.
//

import UIKit
class BorderButton: UIButton {

    func customizeView() {
        layer.shadowOpacity = 0.4
        layer.shadowOffset = CGSize(width: 2, height: 2)
        layer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.customizeView()
    }

}
