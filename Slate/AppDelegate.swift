//
//  AppDelegate.swift
//  Slate
//
//  Created by Eric Scaramuzzo on 11/28/17.
//  Copyright © 2017 Eric Scaramuzzo. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        fetch()
        return true
    }
    
    func fetch() {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
        
        let fetchRequest = NSFetchRequest<Data>(entityName: "Data")
        do {
            let tempData = try managedContext.fetch(fetchRequest)
            for d in tempData {
                for c in d.courses! {
                    if let course = c as? CourseData{
                        let newCourse = Course(forName: course.name!, forId: course.id!)
                        if course.categories != nil {
                            let categories = course.categories
                            for category in categories! {
                                if let categoryData = category as? CategoryData {
                                    newCourse.addCategory(category: Category(name: categoryData.name!, weight: categoryData.weight))
                                }
                            }
                        }
                        if course.assignments != nil {
                            let assignments = course.assignments
                            for assignment in assignments! {
                                if let assignmentData = assignment as? AssignmentData {
                                    newCourse.addAssignment(assignment: Assignment(forName: assignmentData.name!, forCategory: Category(name: (assignmentData.category?.name!)!, weight: (assignmentData.category?.weight)!), forPossiblePoints: assignmentData.possiblePoints, forDescription: assignmentData.descriptionString!))
                                }
                            }
                        }
                        if course.students != nil {
                            let students = course.students
                            for student in students! {
                                if let studentData = student as? StudentData {
                                    let newStudent = Student(forName: studentData.name!, forID: studentData.id!)
                                    newCourse.addStudent(student: newStudent)
                                    newStudent.totalGrade = studentData.totalGrade
                                    if let studentGrades = studentData.grades?.allObjects as? [GradeData] {
                                        if newCourse.assignments.count > 0 {
                                            for index in 0...newCourse.assignments.count - 1{
                                                for s in studentGrades {
                                                    if newCourse.assignments[index].name == s.assignmentName {
                                                        newStudent.grades[newCourse.assignments[index]] = s.grade
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if course.criterias!.count > 5 {
                            newCourse.gradeCriterias = []
                            for criteria in course.criterias! {
                                let criteriaData = criteria as? GradeCriteriaData
                                let newCriteria = GradeCriteria(lowerBound: (criteriaData?.lowerBound)!, upperBound: (criteriaData?.upperBound)!, name: (criteriaData?.name)!)
                                newCourse.gradeCriterias.append(newCriteria)
                            }
                            newCourse.gradeCriterias = newCourse.gradeCriterias.sorted(by: { (g1, g2) -> Bool in
                                return g1.lowerBound > g2.lowerBound
                            })
                        }
                        for attendanceData in course.attendance! {
                            if let attendance = attendanceData as? AttendanceData {
                                let newAttendance = Attendance()
                                newAttendance.dateTaken = attendance.dateTaken!
                                var attendances : [String:Bool] = [:]
                                for singleAttendance in attendance.attendances! {
                                    if let singleAttendanceData = singleAttendance as? SingleAttendanceData {
                                        attendances[singleAttendanceData.id!] = singleAttendanceData.value
                                    }
                                }
                                newAttendance.attendance = attendances
                                newCourse.attendances.append(newAttendance)
                            }
                        }
                        DataService.instance.courses.append(newCourse)
                    }
                }
            }
        } catch {
            
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        DataService.instance.save()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "slate")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }


}

