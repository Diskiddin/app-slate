//
//  StudentVC.swift
//  Slate
//
//  Created by Eric Scaramuzzo on 11/29/17.
//  Copyright © 2017 Eric Scaramuzzo. All rights reserved.
//

import UIKit

class StudentsVC: UIViewController, UITableViewDelegate, UITableViewDataSource, AddStudentProtocol {
    
    var course : Course = DataService.instance.selectedCourse!
    @IBOutlet weak var studentTableView: UITableView!
    var students : [Student]!
    var deleteStudentPath : IndexPath = IndexPath()

    override func viewDidLoad() {
        super.viewDidLoad()
        studentTableView.delegate = self
        studentTableView.dataSource = self
        
        students = course.students
        
        studentTableView.allowsSelectionDuringEditing = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        students = course.students
        studentTableView.reloadData()
        DispatchQueue.global(qos: .userInitiated).async {
            DataService.instance.save()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if course.students.count > 0 {
            return true
        } else {
            return false
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if students.count == 0 {
            return 1
        }
        return students.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if students.count > 0 {
            DataService.instance.selectedStudent = students[indexPath.section]
            performSegue(withIdentifier: "StudentSegue", sender: self)
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            deleteStudentPath = indexPath
            let studentToDelete = students[indexPath.section]
            confirmDelete(student: studentToDelete)
        }
    }
    
    func confirmDelete(student : Student) {
        let alertView = UIAlertController(title: "Delete Student", message: "Are you sure you want to delete student: \(student.name!)?", preferredStyle: .actionSheet)
        
        let deleteAction = UIAlertAction(title: "Delete", style: .destructive) { (result) in
            let index = DataService.instance.selectedCourse?.students.index(of: student)
            DataService.instance.selectedCourse?.students.remove(at: index!)
            self.course.students = self.course.students.sorted(by: { (s1, s2) -> Bool in
                return s1.name < s2.name
            })
            DataService.instance.changesMade = true
            DispatchQueue.global(qos: .userInitiated).async {
                DataService.instance.save()
            }
            self.reloadDataInStudentTable()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (result) in
            self.deleteStudentPath = IndexPath()
        }
        
        alertView.addAction(cancelAction)
        alertView.addAction(deleteAction)
        
        present(alertView, animated: true, completion: nil)
    }
    
    func reloadDataInStudentTable() {
        students = course.students
        studentTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if course.students.count == 0 {
            let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
            cell.textLabel?.text = "No Students"
            cell.textLabel?.font = UIFont(name: "Avenir Next", size: 25)
            cell.selectionStyle = .none
            return cell
        }
        if let cell = tableView.dequeueReusableCell(withIdentifier: "StudentCell") as? StudentCell {
            cell.student = students[indexPath.section]
            cell.updateViews()
            return cell
        } else {
            return StudentCell()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let addStudentVC = segue.destination as? CreateStudentVC {
            addStudentVC.course = self.course
        }
    }
    
    func updateStudents() {
        reloadDataInStudentTable()
    }
    

    
}
