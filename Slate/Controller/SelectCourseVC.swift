//
//  SelectCourseVC.swift
//  Slate
//
//  Created by Eric Scaramuzzo on 12/3/17.
//  Copyright © 2017 Eric Scaramuzzo. All rights reserved.
//

import UIKit

protocol selectCourseProtocol {
    func update(course: Course)
}

class SelectCourseVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    

    @IBOutlet weak var courseTableView: UITableView!
    
    var courses:[Course] = DataService.instance.courses
    
    var protocolDelegate:selectCourseProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        courseTableView.delegate = self
        courseTableView.dataSource = self

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return courses.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        protocolDelegate?.update(course: courses[indexPath.row])
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell") {
            cell.textLabel?.text = courses[indexPath.row].getName()
            cell.detailTextLabel?.text = "Id: " + courses[indexPath.row].getId()
            return cell
        } else {
            return UITableViewCell()
        }
    }

}
