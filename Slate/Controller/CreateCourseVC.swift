//
//  CreateCourseVC.swift
//  Slate
//
//  Created by Eric Scaramuzzo on 11/28/17.
//  Copyright © 2017 Eric Scaramuzzo. All rights reserved.
//

import UIKit

protocol updateCourseTable {
    func update()
}

class CreateCourseVC: UIViewController, UITextFieldDelegate, UIPopoverPresentationControllerDelegate, selectCourseProtocol {
    

    @IBOutlet weak var nameInput: UITextField!
    @IBOutlet weak var courseIdInput: UITextField!
    
    @IBOutlet weak var blankIdWarning: UILabel!
    @IBOutlet weak var uniqueCourseIdWarning: UILabel!
    @IBOutlet weak var blankNameWarning: UILabel!
    
    var importCourse: Course?
    var protocolDelegate:updateCourseTable!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        self.nameInput.delegate = self
        
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        nameInput.endEditing(true)
        courseIdInput.becomeFirstResponder()
        return true
    }
    
    @IBAction func createButtonPressed(_ sender: Any) {
        
        var isIdUnique = false
        var isNameBlank = true
        var isIdBlank = true
        
        uniqueCourseIdWarning.isHidden = true
        blankIdWarning.isHidden = true
        blankNameWarning.isHidden = true
        
        if DataService.instance.getId().count == 0 {
            isIdUnique = true
        } else {
            for s in DataService.instance.getId() {
                if s == courseIdInput.text {
                    uniqueCourseIdWarning.isHidden = false
                    isIdUnique = false
                    return
                }
                isIdUnique = true
                uniqueCourseIdWarning.isHidden = true
            }
        }
        

        
        if nameInput.text == "" {
            blankNameWarning.isHidden = false
            isNameBlank = true
            return
        } else {
            blankNameWarning.isHidden = true
            isNameBlank = false
        }
            
            
        if courseIdInput.text == "" {
            blankIdWarning.isHidden = false
            isIdBlank = true
            return
        } else {
            blankIdWarning.isHidden = true
            isIdBlank = false
        }
        
        if isIdUnique && !isIdBlank && !isNameBlank {
            let newCourse = Course(forName: nameInput.text!, forId: courseIdInput.text!)
            if let students = importCourse?.students {
                for s in students {
                    newCourse.addStudent(student: Student(forName: s.name, forID: s.id))
                }
            }
            DataService.instance.courses.append(newCourse)
            DataService.instance.changesMade = true
            DispatchQueue.global(qos: .userInitiated).async {
                DataService.instance.save()
            }
            protocolDelegate.update()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func back(sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    
    func update(course: Course) {
        importCourse = course
    }
    

}
