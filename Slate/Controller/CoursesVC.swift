//
//  ViewController.swift
//  Slate
//
//  Created by Eric Scaramuzzo on 11/28/17.
//  Copyright © 2017 Eric Scaramuzzo. All rights reserved.
//

import UIKit
import CoreData
import Fakery

let appDelegate = UIApplication.shared.delegate as? AppDelegate

class CoursesVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPopoverPresentationControllerDelegate, updateCourseTable{
    
    @IBOutlet weak var courseTable: UITableView!
    @IBOutlet var addButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        courseTable.delegate = self
        courseTable.dataSource = self
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Populate", style: .plain, target: self, action: #selector(CoursesVC.addABunchOfClasses))
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        DataService.instance.courses.sort { (course1, course2) -> Bool in
            return course1.getName() < course2.getName()
        }
        courseTable.reloadData()
    }
    
    
    @objc func addABunchOfClasses() {
        
        let faker = Faker(locale: "en-US")
        
        var id = faker.number.randomInt()
    
        while DataService.instance.getId().contains(String(id)) {
            id = faker.number.randomInt()
        }
        let newCourse = Course(forName: "Course", forId: "\(id)")
        for s in 1...50 {
            newCourse.addStudent(student: Student(forName: faker.name.firstName(), forID: "\(s)"))
        }
        let hwCategory = Category(name: "HW", weight: 20)
        let testsCategory = Category(name: "Tests", weight: 30)
        let quizzesCategory = Category(name: "Quizzes", weight: 20)
        let finalCategory = Category(name: "Final", weight: 30)
        newCourse.addCategory(category: hwCategory)
        newCourse.addCategory(category: testsCategory)
        newCourse.addCategory(category: quizzesCategory)
        newCourse.addCategory(category: finalCategory)
        
        for i in 1...4 {
            newCourse.addAssignment(assignment: Assignment(forName: "Hw \(i)", forCategory: hwCategory, forPossiblePoints: 20, forDescription: ""))
            newCourse.addAssignment(assignment: Assignment(forName: "Test \(i)", forCategory: testsCategory, forPossiblePoints: 25, forDescription: ""))
            newCourse.addAssignment(assignment: Assignment(forName: "Quiz \(i)", forCategory: quizzesCategory, forPossiblePoints: 15, forDescription: ""))
        }
        newCourse.addAssignment(assignment: Assignment(forName: "Final", forCategory: finalCategory, forPossiblePoints: 50, forDescription: ""))
        
        for a in newCourse.assignments {
            for s in newCourse.students {
                s.grades[a] = faker.number.randomDouble(min: a.possiblePoints / 1.5, max: a.possiblePoints)
            }
        }
        DataService.instance.courses.append(newCourse)
        courseTable.reloadData()
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return DataService.instance.courses.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as? CourseCell
        DataService.instance.selectedCourse = cell?.course
        performSegue(withIdentifier: "SelectCourseSegue", sender: cell?.course)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CourseCell") as? CourseCell {
            let course = DataService.instance.courses[indexPath.section]
            cell.course = course
            cell.updateView()
            return cell
        } else {
            return CourseCell()
        }
    }
    @IBAction func createCourseButton(_ sender: Any) {
        let popover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CreateCourseVC") as! CreateCourseVC
        popover.modalPresentationStyle = UIModalPresentationStyle.popover
        popover.popoverPresentationController?.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        popover.popoverPresentationController?.delegate = self
        popover.protocolDelegate = self
        popover.popoverPresentationController?.barButtonItem = addButton
        popover.popoverPresentationController?.permittedArrowDirections = .up
        popover.preferredContentSize = CGSize(width: 304, height: 170)
        self.present(popover, animated: true, completion: nil)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let courseVC = segue.destination as? CourseVC {
            assert(sender as? Course != nil)
            let course = sender as! Course
            courseVC.navigationItem.title = course.getName()
            courseVC.course = course
            let backItem = UIBarButtonItem()
            backItem.title = "Courses"
            navigationItem.backBarButtonItem = backItem
        }
    }
    
    func fetch(completion: (_ complete: Bool) ->()) {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
        
        let fetchRequest = NSFetchRequest<Data>(entityName: "Data")
        do {
            let tempData = try managedContext.fetch(fetchRequest)
            for d in tempData {
                for c in d.courses! {
                    if let course = c as? CourseData{
                        DataService.instance.courses.append(Course(forName: course.name!, forId: course.id!))
                    }
                }
            }
        } catch {
            
        }
    }
    
    func update() {
        DataService.instance.courses.sort { (c1, c2) -> Bool in
            return c1.getName() < c2.getName()
        }
        courseTable.reloadData()
    }
    
    func delete() {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else {return}
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Data")
        do {
            print(try managedContext.execute(NSBatchDeleteRequest(fetchRequest: fetchRequest)))
        } catch {
            
        }
    }
    

}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}


