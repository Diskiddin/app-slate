//
//  SelectCategoryVC.swift
//  Slate
//
//  Created by Eric Scaramuzzo on 11/30/17.
//  Copyright © 2017 Eric Scaramuzzo. All rights reserved.
//

import UIKit

protocol updateCategoryView {
    func update()
}

class SelectCategoryVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPopoverPresentationControllerDelegate  {
    
    var category: Category!
    var myProtocol:updateCategoryView?
    var categories = DataService.instance.selectedCourse!.categories

    @IBOutlet weak var addCategoryButton: UIButton!
    @IBOutlet weak var categoriesTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        categoriesTableView.delegate = self
        categoriesTableView.dataSource = self
        categories = categories.sorted { (c1, c2) -> Bool in
            return c1.name < c2.name
        }
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count + 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == categories.count {
            DataService.instance.selectedCategory = DataService.instance.selectedCourse?.unassignedCategory
            myProtocol?.update()
            self.dismiss(animated: true, completion: nil)
            return
        }
        DataService.instance.selectedCategory = categories[indexPath.row]
        myProtocol?.update()
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell"){
            if indexPath.row == categories.count {
                cell.textLabel?.text = DataService.instance.selectedCourse?.unassignedCategory.name
                cell.textLabel?.textColor = UIColor.red
                return cell
            }
            cell.textLabel?.text = categories[indexPath.row].name + " " + String(categories[indexPath.row].weight)
            return cell
        } else {
            return UITableViewCell()
        }
        
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    @IBAction func addCategoryButton(_ sender: Any) {
        let popover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CreateCategoryPopover") as! CreateCategoryVC
        popover.modalPresentationStyle = UIModalPresentationStyle.popover
        popover.popoverPresentationController?.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        popover.popoverPresentationController?.delegate = self
        popover.popoverPresentationController?.sourceView = view
        popover.popoverPresentationController?.sourceRect = view.bounds
        popover.popoverPresentationController?.permittedArrowDirections = .any
        popover.preferredContentSize = CGSize(width: 317, height: 150)
        self.present(popover, animated: true, completion: nil)
    }
    
    

}
