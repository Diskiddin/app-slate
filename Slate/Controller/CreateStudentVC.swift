//
//  CreateStudentVC.swift
//  Slate
//
//  Created by Eric Scaramuzzo on 11/30/17.
//  Copyright © 2017 Eric Scaramuzzo. All rights reserved.
//

import UIKit

protocol AddStudentProtocol {
    func updateStudents()
}

class CreateStudentVC: UIViewController {
    
    var course : Course!
    var delegateProtocol:AddStudentProtocol!

    @IBOutlet weak var studentNameInput: UITextField!
    @IBOutlet weak var studentIdInput: UITextField!
    @IBOutlet weak var studentNameBlankWarning: UILabel!
    @IBOutlet weak var studentIdNotUniqueWarning: UILabel!
    @IBOutlet weak var studentIdBlankWarning: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(CreateStudentVC.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
    }
    
    @objc func back(sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func createButton(_ sender: Any) {
        
        var nameIsNotBlank = true
        var idIsNotBlank = true
        var idIsUnique = true
        
        if studentNameInput.text == "" {
            nameIsNotBlank = false
            studentNameBlankWarning.isHidden = false
            return
        } else {
            studentNameBlankWarning.isHidden = true
        }
        
        if studentIdInput.text == "" {
            idIsNotBlank = false
            studentIdBlankWarning.isHidden = false
            return
        } else {
            studentIdBlankWarning.isHidden = true
        }
        
        for s in course.students {
            if s.id == studentIdInput.text {
                idIsUnique = false
                studentIdNotUniqueWarning.isHidden = false
                return
            }
            studentIdNotUniqueWarning.isHidden = true
        }
        
        if nameIsNotBlank && idIsNotBlank && idIsUnique {
            course.addStudent(student: Student(forName: studentNameInput.text!, forID: studentIdInput.text!))
            course.students = course.students.sorted(by: { (s1, s2) -> Bool in
                return s1.name < s2.name
            })
            DataService.instance.changesMade = true
            DispatchQueue.global(qos: .userInitiated).async {
                DataService.instance.save()
                DispatchQueue.main.async {
                    self.delegateProtocol.updateStudents()
                }
            }
            studentNameInput.text = ""
            studentIdInput.text = ""
        }
    }
}
