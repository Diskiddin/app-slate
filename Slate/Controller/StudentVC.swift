//
//  StudentVC.swift
//  Slate
//
//  Created by Eric Scaramuzzo on 12/4/17.
//  Copyright © 2017 Eric Scaramuzzo. All rights reserved.
//

import UIKit

class StudentVC: UIViewController, UITableViewDataSource, UITextFieldDelegate, changeAssignmentTextField {
    

    @IBOutlet var studentNameTextField: UITextField!
    @IBOutlet weak var studentIdLabel: UILabel!
    @IBOutlet weak var studentGradeLabel: UILabel!
    @IBOutlet weak var assignmentsTableView: UITableView!
    @IBOutlet var blurView: UIVisualEffectView!
    @IBOutlet var studentGradeInputFieldTemp: UITextField!
    @IBOutlet var newGradeLabel: UILabel!
    @IBOutlet var studentLetterGradeLabel: UILabel!
    @IBOutlet var attendanceLabel: UILabel!
    
    
    var keyBoardHeight : CGFloat = 0
    var student = DataService.instance.selectedStudent
    var assignmentGradeToChange:Assignment!
    var calculateHeight = true;
    var tempName: String!
    var editButton : UIBarButtonItem!
    var doneButton : UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        studentNameTextField.text = student?.name
        studentIdLabel.text = "Id: " + (student?.id)!
        
        studentGradeInputFieldTemp.delegate = self
        studentNameTextField.delegate = self
        assignmentsTableView.dataSource = self
        
        var totalAttendances = 0
        var totalAttendancesPresent = 0
        
        for attendance in DataService.instance.selectedCourse!.attendances {
            if attendance.attendance.keys.contains(student!.id) {
                totalAttendances += 1
                if attendance.attendance[student!.id] == true {
                    totalAttendancesPresent += 1
                }
            }
        }
        
        attendanceLabel.text = "Attendance: \(totalAttendancesPresent)/\(totalAttendances)"
        
        let grade = DataService.instance.selectedCourse?.calculateGrade(student: student!)
        let letterGrade = DataService.instance.selectedCourse?.getLetterGrade(student: student!)
        
        studentLetterGradeLabel.text = letterGrade
        studentGradeLabel.text = String(format: "%.2f%%",grade!)
        
        editButton = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(StudentVC.edit))
        navigationItem.rightBarButtonItem = editButton
        
        DataService.instance.selectedCourse?.assignments = (DataService.instance.selectedCourse?.assignments.sorted(by: { (a1, a2) -> Bool in
            return a1.name < a2.name
        }))!
        self.hideKeyboardWhenTappedAround()
        
    }
    
    @objc func edit() {
        studentNameTextField.borderStyle = .roundedRect
        studentNameTextField.isEnabled = true
        isEditing = true
        doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(StudentVC.doneEditing))
        navigationItem.rightBarButtonItem = doneButton
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        doneEditing()
    }
    
    @objc func doneEditing() {
        studentNameTextField.borderStyle = .none
        studentNameTextField.isEnabled = false
        editButton = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(StudentVC.edit))
        isEditing = false
        navigationItem.rightBarButtonItem = editButton
    }
    
    @objc func doneEditingAssignment(){
        studentGradeInputFieldTemp.endEditing(true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == studentGradeInputFieldTemp{
            if let index = textField.text?.index(of: "."){
                if string == "." {
                    return false
                }
                if range.location > index.encodedOffset + 2 {
                    return false
                }
            } else {
                if range.location + string.count > 3 && string != "."{
                    return false
                }
                return true
            }
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == studentGradeInputFieldTemp {
            UIView.animate(withDuration: 0.5, animations: {
                self.blurView.alpha = 0.0
            })
            studentGradeInputFieldTemp.isHidden = true
            newGradeLabel.isHidden = true
            guard Double(textField.text!) != nil else {
                if isEditing {
                    doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(StudentVC.doneEditing))
                    navigationItem.rightBarButtonItem = doneButton
                } else {
                    editButton = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(StudentVC.edit))
                    navigationItem.rightBarButtonItem = editButton
                }
                return
            }
            student?.grades[assignmentGradeToChange] = Double(textField.text!)!
            assignmentsTableView.reloadData()
            let grade = DataService.instance.selectedCourse?.calculateGrade(student: student!)
            studentGradeLabel.text = String(format: "%.2f%%", grade!)
            studentLetterGradeLabel.text = DataService.instance.selectedCourse?.getLetterGrade(student: student!)
            studentGradeInputFieldTemp.text = ""
            DataService.instance.changesMade = true
            DispatchQueue.global(qos: .userInitiated).async {
                DataService.instance.save()
            }
            if isEditing {
                doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(StudentVC.doneEditing))
                navigationItem.rightBarButtonItem = doneButton
            } else {
                editButton = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(StudentVC.edit))
                navigationItem.rightBarButtonItem = editButton
            }
        } else if textField == studentNameTextField {
            if textField.text != "" {
                student?.setName(name: textField.text!)
                DataService.instance.changesMade = true
                DispatchQueue.global(qos: .userInitiated).async {
                    DataService.instance.save()
                }
            } else {
                textField.text = tempName
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (student?.grades.keys.count)!
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == studentNameTextField {
            tempName = textField.text
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "assignmentCell") as? AssignmentStudentCell {
            let assignment = DataService.instance.selectedCourse?.assignments[indexPath.row]
            cell.updateViews(name: (assignment?.name)!, category: (assignment?.category.name)!, grade: (student?.grades[assignment!])!, totalGrade: (assignment?.possiblePoints)!)
            cell.protocolDelegate = self
            cell.assignment = assignment
            return cell
        } else {
            return AssignmentStudentCell()
        }
    }
    
    func changeTextField(assignment: Assignment) {
        assignmentGradeToChange = assignment
        studentGradeInputFieldTemp.becomeFirstResponder()
        studentGradeInputFieldTemp.isHidden = false
        newGradeLabel.isHidden = false
        newGradeLabel.text = assignment.name + " Grade: "
        doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(StudentVC.doneEditingAssignment))
        UIView.animate(withDuration: 0.5) {
            self.blurView.alpha = 1.0
        }
        navigationItem.rightBarButtonItem = doneButton
        studentGradeInputFieldTemp.text = ""
    }
    
    

}
