//
//  CreateCategoryVC.swift
//  Slate
//
//  Created by Eric Scaramuzzo on 12/3/17.
//  Copyright © 2017 Eric Scaramuzzo. All rights reserved.
//

import UIKit

protocol updateView {
    func update()
}

class CreateCategoryVC: UIViewController, UITextFieldDelegate {

    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var weightTextField: UITextField!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var weightLabel: UILabel!
    
    var protocolDelegate:updateView!
    var doneProtocolDelete:gradeCriteriaProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        weightTextField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        nameTextField.text = ""
        weightTextField.text = ""
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == weightTextField{
            if let index = textField.text?.index(of: "."){
                if string == "." {
                    return false
                }
                if range.location > index.encodedOffset + 2 {
                    return false
                }
            } else {
                if range.location + string.count > 3 && string != "."{
                    return false
                }
                return true
            }
        }
        return true
    }
    
    
    @IBAction func createButtonAction(_ sender: Any) {
        if nameTextField.text == "" {
            nameLabel.textColor = UIColor.red
            nameTextField.becomeFirstResponder()
            return
        } else {
            nameLabel.textColor = UIColor.black
        }
        if (DataService.instance.selectedCourse?.getCategoryIds().contains(nameTextField.text!))! {
            nameLabel.textColor = UIColor.red
            nameTextField.becomeFirstResponder()
            return
        } else {
            nameLabel.textColor = UIColor.black
        }
        guard let weight = Double(weightTextField.text!) else {
            weightLabel.textColor = UIColor.red
            weightTextField.becomeFirstResponder()
            return
        }
        weightLabel.textColor = UIColor.black
        let newCategory = Category(name: nameTextField.text!, weight: weight)
        DataService.instance.selectedCourse?.addCategory(category: newCategory)
        protocolDelegate.update()
        DataService.instance.changesMade = true
        DispatchQueue.global(qos: .userInitiated).async {
            DataService.instance.save()
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        doneProtocolDelete.done()
    }
}
