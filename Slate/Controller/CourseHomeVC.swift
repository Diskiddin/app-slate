//
//  CourseHomeVC.swift
//  Slate
//
//  Created by Eric Scaramuzzo on 12/4/17.
//  Copyright © 2017 Eric Scaramuzzo. All rights reserved.
//

import UIKit

class CourseHomeVC: UIViewController, UITextFieldDelegate, UIPopoverPresentationControllerDelegate, updateView, changeCategoryWeightProtocol, gradeCriteriaProtocol {
    
    
    
    @IBOutlet var courseNameTextField: UITextField!
    @IBOutlet var courseIdLabel: UILabel!
    @IBOutlet var courseEnrollmentLabel: UILabel!
    @IBOutlet var gradeCriteriaButton: BorderButton!
    @IBOutlet var viewCourseStatisticsButton: UIButton!
    @IBOutlet var blurView: UIVisualEffectView!
    
    var course: Course = DataService.instance.selectedCourse!
    var categoryToChange:Category!
    var tempName:String!
    var oldWeight:Double!
    var gradeCriteriaPopover:GradeCriteriaVC!
    var categoryCreatePopover:CreateCategoryVC!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        isEditing = false

        courseNameTextField.delegate = self
        
        course.categories = course.categories.sorted(by: { (c1, c2) -> Bool in
            return c1.name < c2.name
        })
        
        courseNameTextField.text = course.getName()
        courseIdLabel.text = "Id: " + course.getId()
        courseEnrollmentLabel.text = String(format: "Students Enrolled: %d", course.students.count)
        
        viewCourseStatisticsButton.titleLabel?.numberOfLines = 0
        viewCourseStatisticsButton.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping;
        
//        let disabledTitle = NSMutableAttributedString(string: "View Course Statistics", attributes: [NSAttributedStringKey.foregroundColor : #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1), NSAttributedStringKey.font : UIFont.init(name: "AvenirNext-Medium", size: 28)])
//
//        let enabledTitle = NSMutableAttributedString(string: "View Course Statistics", attributes: [NSAttributedStringKey.foregroundColor : #colorLiteral(red: 0.937254902, green: 0.5568627451, blue: 0.4823529412, alpha: 1), NSAttributedStringKey.font : UIFont.init(name: "AvenirNext-Medium", size: 28)])
//
//        viewCourseStatisticsButton.setAttributedTitle(disabledTitle, for: .disabled)
//        viewCourseStatisticsButton.setAttributedTitle(enabledTitle, for: .normal)
        
        
        gradeCriteriaPopover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LetterGradeCriteriaVC") as! GradeCriteriaVC
        categoryCreatePopover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CreateCategoryPopover") as! CreateCategoryVC
        updateTotalWeight()
        
    }
    
    func editing(){
        isEditing = true
        updateTotalWeight()
    }
    
    func doneEditing(){
        isEditing = false
        updateTotalWeight()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        courseEnrollmentLabel.text = String(format: "Students Enrolled: %d", course.students.count)
//        if course.students.count == 0 {
//            viewCourseStatisticsButton.isEnabled = false
//        } else {
//            viewCourseStatisticsButton.isEnabled = true
//        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" {
            textField.text = tempName
        } else {
            course.setName(name: textField.text!)
            DataService.instance.changesMade = true
            DispatchQueue.global(qos: .userInitiated).async {
                DataService.instance.save()
            }
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == courseNameTextField {
            tempName = textField.text
        }
    }
    
    func updateTotalWeight() {
        var totalWeight = 0.0
        for c in course.categories {
            totalWeight += c.weight
        }
    }
    
    func updateGradesAndStatistics() {
        DispatchQueue.global(qos: .userInitiated).async {
            for s in self.course.students {
                _ = self.course.calculateGrade(student: s)
            }
        }
    }
    
    func update() {
        course.categories = course.categories.sorted(by: { (c1, c2) -> Bool in
            return c1.name < c2.name
        })
        updateTotalWeight()
    }
    
    func changeCategoryWeight(category: Category) {
        fadeBlurView()
        categoryToChange = category
        oldWeight = category.weight
    }
    
    @IBAction func gradeCriteriaButton(_ sender: Any) {
        gradeCriteriaPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        gradeCriteriaPopover.popoverPresentationController?.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        gradeCriteriaPopover.popoverPresentationController?.delegate = self
        gradeCriteriaPopover.protocolDelegate = self
        gradeCriteriaPopover.popoverPresentationController?.sourceView = courseNameTextField
        gradeCriteriaPopover.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
        gradeCriteriaPopover.preferredContentSize = CGSize(width: view.frame.width, height: view.frame.height)
        gradeCriteriaPopover.oldHeight = view.frame.height
        fadeBlurView()
        self.present(gradeCriteriaPopover, animated: true, completion: nil)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        if popoverPresentationController == gradeCriteriaPopover.popoverPresentationController {
            return false
        } else {
            return true
        }
    }
    
    func fadeBlurView() {
        blurView.alpha = 0.0
        UIView.animate(withDuration: 0.3, animations: {
            self.blurView.alpha = 1.0
        })
    }
    
    func done() {
        UIView.animate(withDuration: 0.3, animations: {
            self.blurView.alpha = 0.0
        })
    }
    
    @IBAction func showCourseStatisticsAction(_ sender: Any) {
        performSegue(withIdentifier: "CourseStatisticsSegue", sender: self)
    }
    
    
    @IBAction func viewAttendanceAction(_ sender: Any) {
        performSegue(withIdentifier: "AttendanceSegue", sender: self)
    }
    
}
