//
//  SelectSortAssignmentsVC.swift
//  Slate
//
//  Created by Eric Scaramuzzo on 12/21/17.
//  Copyright © 2017 Eric Scaramuzzo. All rights reserved.
//

import UIKit

protocol SortAssignmentTable {
    func sortTable(sortBy: String)
}

class SortAssignmentsVC: UIViewController {
    
    var protocolDelegate:SortAssignmentTable!

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func sortByCategoryAction(_ sender: Any) {
        protocolDelegate.sortTable(sortBy: "Category")
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    @IBAction func sortByNameAction(_ sender: Any) {
        
        protocolDelegate.sortTable(sortBy: "Name")
        self.dismiss(animated: true, completion: nil)
    }
    
}
