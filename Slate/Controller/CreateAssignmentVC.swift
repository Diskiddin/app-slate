//
//  CreateAssignmentVC.swift
//  Slate
//
//  Created by Eric Scaramuzzo on 11/30/17.
//  Copyright © 2017 Eric Scaramuzzo. All rights reserved.
//

import UIKit

class CreateAssignmentVC: UIViewController, UIPopoverPresentationControllerDelegate, UITextFieldDelegate, UITextViewDelegate, updateCategoryView {
    

    @IBOutlet weak var assignmentNameInput: UITextField!
    @IBOutlet weak var selectCategoryButton: BorderButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var assignmentNameBlankWarning: UILabel!
    @IBOutlet weak var assignmentNameNotUniqueWarning: UILabel!
    
    @IBOutlet weak var pointsInput: UITextField!
    
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var createButton: BorderButton!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    
    @IBOutlet weak var descriptionTextField: UITextView!
    @IBOutlet weak var descriptionTextFieldTemp: UITextView!
    
    
    var keyBoardHeight : CGFloat = 0
    var course : Course!
    var selectedCategory : Category?
    var pointsInputAcceptable : Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pointsInput.delegate = self
        descriptionTextField.delegate = self
        self.hideKeyboardWhenTappedAround()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        let userInfo:NSDictionary = notification.userInfo! as NSDictionary
        let keyboardFrame:NSValue = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        let keyboardHeight = keyboardRectangle.height
        self.keyBoardHeight = keyboardHeight
    }
    
    @objc func back(sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func selectCategoryButton(_ sender: Any) {
        let popover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "categoryPopover") as! SelectCategoryVC
        popover.modalPresentationStyle = UIModalPresentationStyle.popover
        popover.popoverPresentationController?.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        popover.popoverPresentationController?.delegate = self
        popover.myProtocol = self
        popover.popoverPresentationController?.sourceView = selectCategoryButton
        popover.popoverPresentationController?.sourceRect = selectCategoryButton.bounds
        popover.popoverPresentationController?.permittedArrowDirections = .any
        popover.preferredContentSize = CGSize(width: 220, height: 170)
        self.present(popover, animated: true, completion: nil)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == descriptionTextField {
                if keyBoardHeight != 0 {
                navigationItem.leftBarButtonItem?.isEnabled = false
                descriptionTextFieldTemp.heightAnchor.constraint(equalToConstant: mainView.frame.height - keyBoardHeight).isActive = true
                descriptionTextFieldTemp.isHidden = false
                view.endEditing(true)
                descriptionTextFieldTemp.becomeFirstResponder()
                navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(CreateAssignmentVC.doneButtonSelected))
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == pointsInput{
            if let index = textField.text?.index(of: "."){
                if string == "." {
                    return false
                }
                if range.location > index.encodedOffset + 2 {
                    return false
                }
            } else {
                if range.location + string.count > 3 && string != "."{
                    return false
                }
                return true
            }
        }
        return true
    }
    
    @objc func doneButtonSelected() {
        descriptionTextFieldTemp.isHidden = true
        descriptionTextField.text = descriptionTextFieldTemp.text
        view.endEditing(true)
        navigationItem.rightBarButtonItem = nil
        navigationItem.leftBarButtonItem?.isEnabled = true
    }
    
    func update() {
        selectedCategory = DataService.instance.selectedCategory
        if selectedCategory != nil {
            selectCategoryButton.setTitle(selectedCategory?.name, for: .normal)
            selectCategoryButton.setTitleColor(#colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1), for: .normal)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == pointsInput {
            var numberOfPeriods = 0
            textField.text?.forEach({ (character) in
                if character == "." {
                    numberOfPeriods += 1
                }
            })
            if numberOfPeriods > 1 {
                pointsInputAcceptable = false
            }
        }
    }
    
    @IBAction func createAssignmentButton(_ sender: Any) {
        
        var nameIsNotBlank = true
        var nameIsUnique = true
        var pointsInputNotBlank = true
        var categorySelected = true
        
        if assignmentNameInput.text == "" {
            nameIsNotBlank = false
            assignmentNameBlankWarning.isHidden = false
        } else {
            assignmentNameBlankWarning.isHidden = true
        }
        
        for a in course.assignments {
            if a.name.lowercased() == assignmentNameInput.text?.lowercased() {
                nameIsUnique = false
                assignmentNameNotUniqueWarning.isHidden = false
                break
            }
            assignmentNameNotUniqueWarning.isHidden = true
        }
        
        if pointsInput.text == nil || pointsInput.text == "." {
            pointsInputNotBlank = false
        } else {
            pointsInputNotBlank = true
        }
        
        if selectedCategory == nil {
            categorySelected = false
            selectCategoryButton.setTitleColor(#colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1), for: .normal)
        } else {
            selectCategoryButton.setTitleColor(#colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1), for: .normal)
            categorySelected = true
        }
        
        if nameIsNotBlank && nameIsUnique && pointsInputNotBlank && pointsInputAcceptable && categorySelected{
            let assignment = Assignment(forName: assignmentNameInput.text!, forCategory: selectedCategory!, forPossiblePoints: Double(pointsInput.text!)!, forDescription: descriptionTextField.text)
            course.addAssignment(assignment: assignment)
            DataService.instance.changesMade = true
            navigationController?.popViewController(animated: true)
        }
        
    }
    
    @objc override func dismissKeyboard() {
        doneButtonSelected()
        view.endEditing(true)
    }
}
