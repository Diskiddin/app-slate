//
//  AssignmentVC.swift
//  Slate
//
//  Created by Eric Scaramuzzo on 12/3/17.
//  Copyright © 2017 Eric Scaramuzzo. All rights reserved.
//

import UIKit

class AssignmentVC: UIViewController, UITextViewDelegate ,UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate,UIPopoverPresentationControllerDelegate, updateCategoryView, changeTextFieldProtocol {
    
    var assignment:Assignment!
    var keyBoardHeight:CGFloat!

    @IBOutlet weak var gradeInputLabelTemp: UITextField!
    @IBOutlet weak var assignmentDescriptionTextView: UITextView!
    @IBOutlet weak var assignmentCategoryButton: BorderButton!
    @IBOutlet weak var assignmentPointsTextField: UITextField!
    @IBOutlet weak var studentTableView: UITableView!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var newGradeLabel: UILabel!
    @IBOutlet var assignmentNameTextField: UITextField!
    @IBOutlet var assignmentMeanLabel: UILabel!
    @IBOutlet var assignmentMinLabel: UILabel!
    @IBOutlet var assignmentMaxLabel: UILabel!
    @IBOutlet var viewAssignmentStatisticsButton: UIButton!
    
    public var cellStudentEditing:Student!
    private var oldPoints:Double!
    public var students: [Student]!
    
    var oldNameText:String!
    var editButton : UIBarButtonItem!
    var doneButton : UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        
        assignment = DataService.instance.selectedAssignment
        
        assignmentPointsTextField.text = String(assignment.possiblePoints)
        assignmentCategoryButton.setTitle(assignment.category.name, for: .normal)
        assignmentDescriptionTextView.text = assignment.description
        assignmentNameTextField.text = assignment.name
        
        doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(AssignmentVC.doneEditing))
        editButton = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(AssignmentVC.edit))
        navigationItem.rightBarButtonItem = editButton
        navigationItem.title = ""
        
        assignmentNameTextField.delegate = self
        studentTableView.delegate = self
        studentTableView.dataSource = self
        assignmentPointsTextField.delegate = self
        gradeInputLabelTemp.delegate = self
        assignmentDescriptionTextView.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        updateStatistics()
        
        students = DataService.instance.selectedCourse?.students
        students.sort { (student1, student2) -> Bool in
            return student1.name < student2.name
        }
        
        if assignment.category == DataService.instance.selectedCourse?.unassignedCategory {
            assignmentCategoryButton.setTitleColor(UIColor.red, for: .normal)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if DataService.instance.selectedCourse?.students.count == 0 {
            viewAssignmentStatisticsButton.isEnabled = false
        }
    }
    
    @objc func edit(){
        assignmentNameTextField.borderStyle = .roundedRect
        assignmentNameTextField.isEnabled = true
        assignmentPointsTextField.borderStyle = .roundedRect
        assignmentPointsTextField.isEnabled = true
        assignmentDescriptionTextView.layer.borderWidth = 1
        assignmentDescriptionTextView.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        assignmentDescriptionTextView.isEditable = true
        viewAssignmentStatisticsButton.isEnabled = false
        doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(AssignmentVC.doneEditing))
        navigationItem.rightBarButtonItem = doneButton
    }
    
    @objc func doneEditing(){
        assignmentNameTextField.borderStyle = .none
        assignmentNameTextField.isEnabled = false
        assignmentPointsTextField.borderStyle = .none
        assignmentPointsTextField.isEnabled = false
        assignmentDescriptionTextView.layer.borderWidth = 0
        assignmentDescriptionTextView.isEditable = false
        viewAssignmentStatisticsButton.isEnabled = true
        editButton = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(AssignmentVC.edit))
        navigationItem.rightBarButtonItem = editButton
    }
    
    
    @objc func keyboardWillShow(notification: Notification) {
        let userInfo:NSDictionary = notification.userInfo! as NSDictionary
        let keyboardFrame:NSValue = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        let keyboardHeight = keyboardRectangle.height
        self.keyBoardHeight = keyboardHeight
    }

    @objc func back() {
        navigationController?.popViewController(animated: true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == assignmentPointsTextField {
            oldPoints = Double(textField.text!)!
        } else if textField == gradeInputLabelTemp {
            UIView.animate(withDuration: 0.5, animations: {
                self.blurView.alpha = 1.0
            })
            newGradeLabel.isHidden = false
            newGradeLabel.text = cellStudentEditing.name + "'s Grade: "
        } else if textField == assignmentNameTextField {
            oldNameText = textField.text
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        assignment.setDescription(description: textView.text)
        DataService.instance.changesMade = true
        DispatchQueue.global(qos: .userInitiated).async {
            DataService.instance.save()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == assignmentPointsTextField || textField == gradeInputLabelTemp{
            if let index = textField.text?.index(of: "."){
                if string == "." {
                    return false
                }
                if range.location > index.encodedOffset + 2 {
                    return false
                }
            } else {
                if range.location + string.count > 3 && string != "."{
                    return false
                }
                return true
            }
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == assignmentPointsTextField {
            guard Double(textField.text!) != nil else {
                textField.text = String(format: "%.2f", oldPoints)
                return
            }
            guard let students = self.students else {
                return
            }
            for s in students {
                let newTotal = Double(textField.text!)!
                let newGrade = Double((s.grades[assignment]! / oldPoints) * newTotal)
                s.grades[assignment] = newGrade
            }
            assignment.setPossiblePoints(points: Double(textField.text!)!)
            updateGradesAndGenerateStatistics()
            DataService.instance.changesMade = true
            DispatchQueue.global(qos: .userInitiated).async {
                DataService.instance.save()
                DispatchQueue.main.async {
                    self.studentTableView.reloadData()
                }
            }
        } else if textField == gradeInputLabelTemp {
            guard Double(textField.text!) != nil else {
                gradeInputLabelTemp.isHidden = true
                UIView.animate(withDuration: 0.5, animations: {
                    self.blurView.alpha = 0.0
                })
                newGradeLabel.isHidden = true
                gradeInputLabelTemp.text = ""
                return
            }
            cellStudentEditing.grades[DataService.instance.selectedAssignment!] = Double(textField.text!)!
            updateGradesAndGenerateStatistics()
            DataService.instance.changesMade = true
            DispatchQueue.global(qos: .userInitiated).async {
                DataService.instance.save()
                DispatchQueue.main.async {
                    self.studentTableView.reloadData()
                }
            }
            textField.text = ""
            gradeInputLabelTemp.isHidden = true
            UIView.animate(withDuration: 0.5, animations: {
                self.blurView.alpha = 0.0
            })
            newGradeLabel.isHidden = true
        } else if textField == assignmentNameTextField {
            if textField.text == "" || (DataService.instance.selectedCourse?.getAssignmentNames().contains(textField.text!))!{
                textField.text = oldNameText
            } else {
                assignment.setName(name: textField.text!)
                DataService.instance.changesMade = true
                DispatchQueue.global(qos: .userInitiated).async {
                    DataService.instance.save()
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return students.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "studentCell") as? StudentAssignmentCell{
            cell.student = students[indexPath.row]
            cell.updateView()
            cell.protocolDelegate = self
            return cell
        } else {
            return StudentAssignmentCell()
        }
    }
    
    func updateGradesAndGenerateStatistics() {
        for s in (DataService.instance.selectedCourse?.students)! {
            let _ = DataService.instance.selectedCourse?.calculateGrade(student: s)
        }
        let statistics = DataService.instance.selectedCourse?.generateStatisticsForAssignment(assignment: assignment)
        assignmentMeanLabel.text = String(format: "%.2f", statistics!["Mean"]!)
        assignmentMinLabel.text = String(format: "%.2f", statistics!["Min"]!)
        assignmentMaxLabel.text = String(format: "%.2f", statistics!["Max"]!)
    }
    
    func changeTextField(student: Student) {
        cellStudentEditing = student
        gradeInputLabelTemp.isHidden = false
        gradeInputLabelTemp.becomeFirstResponder()
    }
    
    func updateStatistics() {
        let statistics = DataService.instance.selectedCourse?.generateStatisticsForAssignment(assignment: assignment)
        assignmentMeanLabel.text = String(format: "%.2f", statistics!["Mean"]!)
        assignmentMinLabel.text = String(format: "%.2f", statistics!["Min"]!)
        assignmentMaxLabel.text = String(format: "%.2f", statistics!["Max"]!)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    @IBAction func assignmentCategoryButtonPressed(_ sender: Any) {
        if (DataService.instance.selectedCourse?.categories.count == 0) {
            noCategoriesAction()
        } else {
            let popover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "categoryPopover") as! SelectCategoryVC
            popover.modalPresentationStyle = UIModalPresentationStyle.popover
            popover.popoverPresentationController?.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            popover.popoverPresentationController?.delegate = self
            popover.myProtocol = self
            popover.popoverPresentationController?.sourceView = assignmentCategoryButton
            popover.popoverPresentationController?.sourceRect = assignmentCategoryButton.bounds
            popover.popoverPresentationController?.permittedArrowDirections = .any
            popover.preferredContentSize = CGSize(width: 220, height: 170)
            self.present(popover, animated: true, completion: nil)
        }
    }
    
    func noCategoriesAction() {
        let alert = UIAlertController(title: "No Categories", message: "Please add a category before assigning an assignment", preferredStyle: .alert)
        
        let addCategoryAction = UIAlertAction(title: "Add Category", style: .default) { (action) in
            alert.dismiss(animated: true, completion: nil )
            self.navigationController?.popViewController(animated: true)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            print("Cancel")
        }
        
        alert.addAction(addCategoryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func update() {
        if DataService.instance.selectedCategory != nil {
            assignment.setCategory(category: DataService.instance.selectedCategory!)
            assignmentCategoryButton.setTitle(assignment.category.name, for: .normal)
            if DataService.instance.selectedCategory == DataService.instance.selectedCourse?.unassignedCategory {
                assignmentCategoryButton.setTitleColor(UIColor.red, for: .normal)
            } else {
                assignmentCategoryButton.setTitleColor(#colorLiteral(red: 0.937254902, green: 0.5568627451, blue: 0.4823529412, alpha: 1), for: .normal)
            }
            DataService.instance.changesMade = true
            DispatchQueue.global(qos: .userInitiated).async {
                DataService.instance.save()
            }
        }
        
    }
    @IBAction func viewAssignmentsAction(_ sender: Any) {
        performSegue(withIdentifier: "AssignmentStatisticsSegue", sender: self)
    }
    
}
