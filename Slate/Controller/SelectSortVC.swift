//
//  SelectSortVC.swift
//  Slate
//
//  Created by Eric Scaramuzzo on 12/8/17.
//  Copyright © 2017 Eric Scaramuzzo. All rights reserved.
//

import UIKit

class SelectSortVC: UIViewController {
    
    var protocolDelegate:AddStudentProtocol!

    @IBAction func gradeButtonAction(_ sender: Any) {
        DataService.instance.selectedCourse?.students = (DataService.instance.selectedCourse?.students.sorted(by: { (s1, s2) -> Bool in
            return s1.totalGrade > s2.totalGrade
        }))!
        protocolDelegate.updateStudents()
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func nameButtonAction(_ sender: Any) {
        DataService.instance.selectedCourse?.students = (DataService.instance.selectedCourse?.students.sorted(by: { (s1, s2) -> Bool in
            return s1.name < s2.name
        }))!
        protocolDelegate.updateStudents()
        dismiss(animated: true, completion: nil)
    }
    
}
