//
//  AssignmentStatisticsVC.swift
//  Slate
//
//  Created by Eric Scaramuzzo on 12/12/17.
//  Copyright © 2017 Eric Scaramuzzo. All rights reserved.
//

import UIKit
import ScrollableGraphView

class AssignmentStatisticsVC: UIViewController, ScrollableGraphViewDataSource {
    
    
    @IBOutlet var graphView: ScrollableGraphView!
    @IBOutlet var assignmentNameLabel: UILabel!
    @IBOutlet var meanView: UIView!
    @IBOutlet var minView: UIView!
    @IBOutlet var maxView: UIView!
    
    var assignment = DataService.instance.selectedAssignment!
    var scores : [Int:Int] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Statistics"
        assignmentNameLabel.text = assignment.name
        
        for s in (DataService.instance.selectedCourse?.students)! {
            let tempGrade = (s.grades[assignment]! / assignment.possiblePoints) * 100
            let roundedGrade = roundDownToTens(tempGrade)
            if !scores.keys.contains(roundedGrade) {
                scores[roundedGrade] = 1
            } else {
                scores[roundedGrade]! += 1
            }
        }
        var maxValue = 0
        for key in scores.keys {
            if scores[key]! > maxValue {
                maxValue = scores[key]!
            }
        }
        
        //let graphFrame = CGRect(x: graphView.frame.origin.x, y: graphView.frame.origin.y, width: graphView.frame.width, height: graphView.frame.height)
        //let graph = ScrollableGraphView(frame: graphFrame, dataSource: self)
        graphView.dataSource = self
        let barPlot = BarPlot(identifier: "bar")
        barPlot.barWidth = 25
        barPlot.barLineWidth = 1
        barPlot.barLineColor = #colorLiteral(red: 0.4666666667, green: 0.4666666667, blue: 0.4666666667, alpha: 1)
        barPlot.barColor = #colorLiteral(red: 0.937254902, green: 0.5568627451, blue: 0.4823529412, alpha: 1)
        barPlot.barLineWidth = 0
        let referenceLines = ReferenceLines()
        referenceLines.referenceLineLabelFont = UIFont(name: "Avenir Next", size: 15)!
        referenceLines.dataPointLabelFont = UIFont(name: "Avenir Next", size: 15)!
        referenceLines.referenceLineLabelColor = #colorLiteral(red: 0.3333333333, green: 0.3333333333, blue: 0.3333333333, alpha: 1)
        referenceLines.dataPointLabelColor = #colorLiteral(red: 0.3333333333, green: 0.3333333333, blue: 0.3333333333, alpha: 1)
        referenceLines.referenceLineThickness = 1
        referenceLines.referenceLineColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        graphView.addReferenceLines(referenceLines: referenceLines)
        graphView.addPlot(plot: barPlot)
        graphView.rangeMin = 0
        if maxValue > 2 {
            //graphView.rangeMax = Double(maxValue) * 1.3
            graphView.rangeMax = Double(findMultipleOfFour(number: maxValue))
            graphView.shouldRangeAlwaysStartAtZero = true
        } else {
            graphView.rangeMax = 2
        }
        graphView.backgroundFillColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
        
        let stats = DataService.instance.selectedCourse!.generateStatisticsForAssignment(assignment: assignment)
        
        var maxStatValue = 0.0
        for key in stats.keys {
            if stats[key]! > maxStatValue {
                maxStatValue = stats[key]!
            }
        }
        
        self.view.addSubview(graphView)
        
        let meanLabelFrame = CGRect(x: 0, y: (meanView.frame.height / 2) - (20/2), width: 48, height: 20)
        let meanLabel = UILabel(frame: meanLabelFrame)
        meanLabel.font = UIFont(name: "Avenir Next", size: 15)
        meanLabel.text = String(format: "%.2f", (stats["Mean"]! / assignment.possiblePoints) * 100)
        
        //let maxWidth = meanView.frame.width - meanLabel.frame.width - 3
        let maxWidth = view.frame.width - meanView.frame.origin.x - 10 - meanLabel.frame.width - 3
        var newMeanWidth = 0.0
        if maxStatValue > 100 {
            newMeanWidth = Double(maxWidth) * (stats["Mean"]! / maxStatValue)
        } else {
            newMeanWidth = Double(maxWidth) * (stats["Mean"]! / assignment.possiblePoints)
        }
        let meanBarFrame = CGRect(x: 0, y: 0, width: 0, height: 25)
        let meanBarView = UIView(frame: meanBarFrame)
        meanBarView.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.5568627451, blue: 0.4823529412, alpha: 1)
        meanView.addSubview(meanBarView)
        meanView.addSubview(meanLabel)
        
        let minLabelFrame = CGRect(x: 0, y: (minView.frame.height / 2) - (20/2), width: 48, height: 20)
        let minLabel = UILabel(frame: minLabelFrame)
        minLabel.font = UIFont(name: "Avenir Next", size: 15)
        minLabel.text = String(format: "%.2f", (stats["Min"]! / assignment.possiblePoints) * 100)
        var newMinWidth = 0.0
        if maxStatValue > 100 {
            newMinWidth = Double(maxWidth) * (stats["Min"]! / maxStatValue)
        } else {
            newMinWidth = Double(maxWidth) * (stats["Min"]! / assignment.possiblePoints)
        }
        let minBarFrame = CGRect(x: 0, y: 0, width: 0, height: 25)
        let minBarView = UIView(frame: minBarFrame)
        minBarView.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.5568627451, blue: 0.4823529412, alpha: 1)
        
        minView.addSubview(minBarView)
        minView.addSubview(minLabel)
        
        let maxlabelFrame = CGRect(x: 0, y: (maxView.frame.height / 2) - (20/2), width: 48, height: 20)
        let maxLabel = UILabel(frame: maxlabelFrame)
        maxLabel.font = UIFont(name: "Avenir", size: 15)
        maxLabel.text = String(format: "%.2f", (stats["Max"]! / assignment.possiblePoints) * 100)
        var newMaxWidth = 0.0
        if maxStatValue > 100 {
            newMaxWidth = Double(maxWidth) * (stats["Max"]! / maxStatValue)
        } else {
            newMaxWidth = Double(maxWidth) * (stats["Max"]! / assignment.possiblePoints)
        }
        let maxBarFrame = CGRect(x: 0, y: 0, width: 0, height: 25)
        let maxBarView = UIView(frame: maxBarFrame)
        maxBarView.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.5568627451, blue: 0.4823529412, alpha: 1)
        
        maxView.addSubview(maxBarView)
        maxView.addSubview(maxLabel)
        
        UIView.animate(withDuration: 1.5, delay: 0, options: .curveEaseInOut, animations: {
            meanBarView.frame = CGRect(x: 0, y: 0, width: newMeanWidth, height: 25)
            meanLabel.frame = CGRect(x: CGFloat(newMeanWidth) + 3.0, y: meanLabel.frame.origin.y, width: meanLabel.frame.width, height: meanLabel.frame.height)
            minBarView.frame = CGRect(x: 0, y: 0, width: newMinWidth, height: 25)
            minLabel.frame = CGRect(x: CGFloat(newMinWidth) + 3.0, y: minLabel.frame.origin.y, width: minLabel.frame.width, height: minLabel.frame.height)
            maxBarView.frame = CGRect(x: 0, y: 0, width: newMaxWidth, height: 25)
            maxLabel.frame = CGRect(x: CGFloat(newMaxWidth) + 3.0, y: maxLabel.frame.origin.y, width: maxLabel.frame.width, height: maxLabel.frame.height)
        }, completion: nil)
        
        
    }
    
    func value(forPlot plot: Plot, atIndex pointIndex: Int) -> Double {
        var keys = Array(scores.keys)
        keys = keys.sorted { (n, n2) -> Bool in
            return n < n2
        }
        return Double(scores[keys[pointIndex]]!)
    }
    
    func label(atIndex pointIndex: Int) -> String {
        var keys = Array(scores.keys)
        keys = keys.sorted { (n, n2) -> Bool in
            return n < n2
        }
        return String(keys[pointIndex])
    }
    
    func numberOfPoints() -> Int {
        return scores.keys.count
    }
    
    func roundDownToTens(_ x : Double) -> Int {
        return 10 * Int((x / 10.0).rounded(FloatingPointRoundingRule.down))
    }
    
    func roundUpToTen(_ x: Double) -> Int {
        return 10 * Int((x / 10.0).rounded(FloatingPointRoundingRule.up))
    }
    
    func findMultipleOfFour(number: Int) -> Int {
        var newNumber = number
        while newNumber % 4 != 0 {
            newNumber += 1
        }
        return newNumber
    }

}
