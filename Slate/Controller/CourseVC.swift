//
//  CourseVC.swift
//  Slate
//
//  Created by Eric Scaramuzzo on 11/29/17.
//  Copyright © 2017 Eric Scaramuzzo. All rights reserved.
//

import UIKit

class CourseVC: UITabBarController, UIPopoverPresentationControllerDelegate {
    
    var courseName: String!
    var courseId: String!
    var course : Course!
    var addButton : UIBarButtonItem!
    var deleteButton : UIBarButtonItem!
    var doneButton : UIBarButtonItem!
    var editButton : UIBarButtonItem!
    var sortButton : UIBarButtonItem!
    var sortAssignmentsButton : UIBarButtonItem!
    var selectedController : String = "Home"

    override func viewDidLoad() {
        super.viewDidLoad()
        deleteButton = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(CourseVC.deleteCourse))
        addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(CourseVC.addButtonAction))
        editButton = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(CourseVC.edit))
        doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(CourseVC.doneEditing))
        sortButton = UIBarButtonItem(title: "Sort By", style: .plain, target: self, action: #selector(CourseVC.showSelectSortPopover))
        sortAssignmentsButton = UIBarButtonItem(title: "Sory By", style: .plain, target: self, action: #selector(CourseVC.sortAssignmentsAction))
        navigationItem.rightBarButtonItems = [deleteButton, editButton]
        
        selectedIndex = 1
        
        let newBackButton = UIBarButtonItem(title: "Home", style: UIBarButtonItemStyle.plain, target: self, action: #selector(AssignmentVC.back))
        
        navigationItem.backBarButtonItem = newBackButton
        
        navigationItem.title = "Home"
        
        
        for item in tabBar.items! {
            var textAttributes : [NSAttributedStringKey : Any] = [:]
            textAttributes[NSAttributedStringKey.font] = UIFont(name: "Avenir Next", size: 17)
            textAttributes[NSAttributedStringKey.backgroundColor] = UIColor.blue
            item.setTitleTextAttributes(textAttributes, for: .normal)
        }
    }
    
    @objc func edit(){
        if selectedController == "Home" {
            let tempController = selectedViewController as? CourseHomeVC
            tempController?.courseNameTextField.borderStyle = .roundedRect
            tempController?.courseNameTextField.isEnabled = true
            tempController?.editing()
            doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(CourseVC.doneEditing))
            navigationItem.rightBarButtonItems = [doneButton]
        }
    }
    
    @objc func doneEditing() {
        if selectedController == "Home" {
            let tempController = selectedViewController as? CourseHomeVC
            tempController?.courseNameTextField.borderStyle = .none
            tempController?.courseNameTextField.isEnabled = false
            tempController?.doneEditing()
            editButton = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(CourseVC.edit))
            deleteButton = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(CourseVC.deleteCourse))
            navigationItem.rightBarButtonItems = [deleteButton,editButton]
        }
    }
    
    @objc func deleteCourse() {
        let alertView = UIAlertController(title: "Delete Course", message: "Are your sure?", preferredStyle: UIAlertControllerStyle.alert)
        let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.default)
        let deleteAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.destructive) { (result) in
            let index = DataService.instance.courses.index(of: self.course)
            DataService.instance.courses.remove(at: index!)
            DataService.instance.changesMade = true
            DispatchQueue.global(qos: .userInitiated).async {
                DataService.instance.save()
            }
            self.navigationController?.popViewController(animated: true)
        }
        alertView.addAction(deleteAction)
        alertView.addAction(cancelAction)
        self.present(alertView, animated: true, completion: nil)
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if item.title == "Students" {
            doneEditing()
            navigationItem.title = "Students"
            selectedController = "Students"
            let newBackButton = UIBarButtonItem(title: "Students", style: UIBarButtonItemStyle.plain, target: self, action: #selector(AssignmentVC.back))
            navigationItem.backBarButtonItem = newBackButton
            self.navigationItem.rightBarButtonItems = [addButton, sortButton]
        }
        if item.title == "Home" {
            navigationItem.title = "Home"
            selectedController = "Home"
            deleteButton = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(CourseVC.deleteCourse))
            editButton = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(CourseVC.edit))
            //let newBackButton = UIBarButtonItem(title: "Home", style: UIBarButtonItemStyle.plain, target: self, action: #selector(AssignmentVC.back))
            self.navigationItem.rightBarButtonItems = [deleteButton,editButton]
        }
        if item.title == "Assignments" {
            doneEditing()
            navigationItem.title = "Assignments"
            selectedController = "Assignments"
            let newBackButton = UIBarButtonItem(title: "Assignments", style: UIBarButtonItemStyle.plain, target: self, action: #selector(AssignmentVC.back))
            navigationItem.backBarButtonItem = newBackButton
            self.navigationItem.rightBarButtonItems = [addButton,sortAssignmentsButton]
        }
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    @objc func showSelectSortPopover() {
            let popover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SelectSortVC") as! SelectSortVC
            popover.modalPresentationStyle = UIModalPresentationStyle.popover
            popover.popoverPresentationController?.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            popover.popoverPresentationController?.delegate = self
            popover.protocolDelegate = selectedViewController as! AddStudentProtocol
            popover.popoverPresentationController?.barButtonItem = sortButton
            popover.popoverPresentationController?.permittedArrowDirections = .up
            popover.preferredContentSize = CGSize(width: 150, height: 59)
            self.present(popover, animated: true, completion: nil)
    }
    
    @objc func sortAssignmentsAction() {
        if selectedController == "Assignments" {
            let popover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SortAssignmentsVC") as! SortAssignmentsVC
            popover.modalPresentationStyle = UIModalPresentationStyle.popover
            popover.popoverPresentationController?.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            popover.popoverPresentationController?.delegate = self
            popover.protocolDelegate = selectedViewController as! SortAssignmentTable
            popover.popoverPresentationController?.barButtonItem = sortAssignmentsButton
            popover.popoverPresentationController?.permittedArrowDirections = .up
            popover.preferredContentSize = CGSize(width: 250, height: 59)
            self.present(popover, animated: true, completion: nil)
        }
        
    }
    
    @objc func addButtonAction() {
        
        if selectedController == "Students" {
            let popover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CreateStudentVC") as! CreateStudentVC
            popover.modalPresentationStyle = UIModalPresentationStyle.popover
            popover.popoverPresentationController?.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            popover.popoverPresentationController?.delegate = self
            popover.course = course
            popover.delegateProtocol = selectedViewController as! AddStudentProtocol
            popover.popoverPresentationController?.sourceView = view
            popover.popoverPresentationController?.sourceRect = view.bounds
            popover.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
            popover.preferredContentSize = CGSize(width: 290, height: 170)
            self.present(popover, animated: true, completion: nil)
        } else if selectedController == "Assignments" {
            if course.assignments.count == 0 {
                course.addAssignment(assignment: Assignment(forName: "New Assignment 1", forCategory: course.unassignedCategory, forPossiblePoints: 100, forDescription: ""))
                (selectedViewController as? AssignmentsVC)?.reloadTableData(sort: false)
                return
            }
            for i in 1...course.assignments.count + 1 {
                var createAssignment = true
                let assignmentName = "New Assignment \(i)"
                for a in course.assignments {
                    if a.name == assignmentName {
                        createAssignment = false
                    }
                }
                if createAssignment {
                    course.addAssignmentAtFromt(assignment: Assignment(forName: assignmentName, forCategory: course.unassignedCategory, forPossiblePoints: 100, forDescription: ""))
                    (selectedViewController as? AssignmentsVC)?.reloadTableData(sort: false)
                    return
                }
            }
//            if course.categories.count == 0 {
//                let noCategoriesAlert = UIAlertController(title: "No Categories", message: "You have no grade categories, please go back to the home page and create one", preferredStyle: .alert)
//
//                let goBackAction = UIAlertAction(title: "Go Back", style: .default, handler: { (result) in
//                    for controller in self.viewControllers! {
//                        if controller.title == "CourseHomeVC" {
//                            self.selectedViewController = controller
//                            (self.selectedViewController as? CourseHomeVC)?.addCategoryAction(self)
//                            return
//                        }
//                    }
//                })
//
//                noCategoriesAlert.addAction(goBackAction)
//
//                present(noCategoriesAlert, animated: true, completion: nil)
//
//            } else {
//                let backItem = UIBarButtonItem()
//                backItem.title = "Back"
//                navigationItem.backBarButtonItem = backItem
//                selectedViewController?.performSegue(withIdentifier: "CreateAssignmentSegue", sender: self)
//            }
        }
    }

}
