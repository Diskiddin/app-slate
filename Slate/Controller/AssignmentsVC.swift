//
//  AssignmentsVC.swift
//  Slate
//
//  Created by Eric Scaramuzzo on 11/30/17.
//  Copyright © 2017 Eric Scaramuzzo. All rights reserved.
//

import UIKit

class AssignmentsVC: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, UIPopoverPresentationControllerDelegate, gradeCriteriaProtocol, changeCategoryWeightProtocol, updateView, SortAssignmentTable{
    
    var course : Course = DataService.instance.selectedCourse!
    var assignments : [Assignment] = []
    var categoryToChange:Category!
    var oldWeight:Double!
    var sortedBy:String!
    var categoryCreatePopover:CreateCategoryVC!

    @IBOutlet var totalWeightLabel: UILabel!
    @IBOutlet var newWeightTextField: UITextField!
    @IBOutlet var newWeightLabel: UILabel!
    @IBOutlet var blurView: UIVisualEffectView!
    @IBOutlet weak var assignmentsTableView: UITableView!
    @IBOutlet var categoryTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assignmentsTableView.delegate = self
        assignmentsTableView.dataSource = self
        categoryTableView.delegate = self
        categoryTableView.dataSource = self
        newWeightTextField.delegate = self
        
        sortedBy = "Name"
        
        self.hideKeyboardWhenTappedAround()
        
        categoryCreatePopover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CreateCategoryPopover") as! CreateCategoryVC
        
        assignments = course.assignments
        assignments.sort { (assignment1, assignment2) -> Bool in
            return assignment1.name < assignment2.name
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        reloadTableData(sort: true)
    }
    
    func changeCategoryWeight(category: Category) {
        UIView.animate(withDuration: 0.6) {
            self.blurView.alpha = 1.0
            self.newWeightLabel.alpha = 1.0
            self.newWeightTextField.alpha = 1.0
            self.newWeightTextField.becomeFirstResponder()
            self.newWeightLabel.text = category.name + " Weight:"
        }
        categoryToChange = category
        oldWeight = category.weight
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        if textField == newWeightTextField {
            guard let newWeight = Double(textField.text!) else {
                UIView.animate(withDuration: 0.6, animations: {
                    self.blurView.alpha = 0.0
                    self.newWeightTextField.alpha = 0.0
                    self.newWeightLabel.alpha = 0.0
                    self.newWeightTextField.text = ""
                })
                return
            }
            categoryToChange.weight = newWeight
            UIView.animate(withDuration: 0.6, animations: {
                self.blurView.alpha = 0.0
                self.newWeightTextField.alpha = 0.0
                self.newWeightLabel.alpha = 0.0
                self.newWeightTextField.text = ""
            })
            DataService.instance.changesMade = true
            DispatchQueue.global(qos: .userInitiated).async {
                DataService.instance.save()
                DispatchQueue.main.async {
                    self.categoryTableView.reloadData()
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if tableView == assignmentsTableView {
            if course.assignments.count > 0 {
                return true
            } else {
                return false
            }
        } else if tableView == categoryTableView {
            if course.categories.count > 0 {
                return true
            } else {
                return false
            }
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if tableView == assignmentsTableView && course.assignments.count > 0 {
            if editingStyle == .delete {
                let assignmentToDelete = course.assignments[indexPath.section]
                deleteAssignment(assignment: assignmentToDelete)
            }
        } else if tableView == categoryTableView && course.categories.count > 0{
            if editingStyle == .delete {
                deleteCategoryAlert(category: course.categories[indexPath.row])
            }
        }
    }
    
    func deleteCategoryAlert(category: Category) {
        let alertView = UIAlertController(title: "Delete Category", message: "Deleting a category will make all assignments in the category unassigned, Continue?", preferredStyle: .actionSheet)
        
        let deleteAction = UIAlertAction(title: "Yes", style: .destructive) { (result) in
            self.deleteCategory(category: category)
        }
        
        let cancelAction = UIAlertAction(title: "No", style: .cancel)
        
        alertView.addAction(cancelAction)
        alertView.addAction(deleteAction)
        
        present(alertView, animated: true, completion: nil)
    }
    
    func deleteCategory(category: Category) {
        for assignment in course.assignments {
            if assignment.category == category {
                assignment.setCategory(category: course.unassignedCategory)
            }
        }
        let index = course.categories.index(of: category)
        course.categories.remove(at: index!)
        DataService.instance.changesMade = true
        DispatchQueue.global(qos: .userInitiated).async {
            DataService.instance.save()
            DispatchQueue.main.async {
                self.categoryTableView.reloadData()
            }
        }
        assignmentsTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == assignmentsTableView {
            return 57
        } else {
            return tableView.rowHeight
        }
    }
    
    
    
    
    func deleteAssignment(assignment: Assignment) {
        let alertView = UIAlertController(title: "Delete Assignment", message: "Are you sure you want to delete \(assignment.name!)?", preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        
        let deleteAction = UIAlertAction(title: "Delete", style: .destructive) { (result) in
            self.course.removeAssignment(assignment: assignment)
            DataService.instance.changesMade = true
            DispatchQueue.global(qos: .userInitiated).async {
                DataService.instance.save()
                DispatchQueue.main.async {
                    self.reloadTableData(sort: false)
                }
            }
        }
        
        alertView.addAction(deleteAction)
        alertView.addAction(cancelAction)
        
        present(alertView, animated: true, completion: nil)
    }
    
    func reloadTableData(sort: Bool) {
        if sort {
            sortTable(sortBy: sortedBy)
//            course.assignments.sort { (assignment1, assignment2) -> Bool in
//                return assignment1.name < assignment2.name
//            }
        }
        assignments = course.assignments
        assignmentsTableView.reloadData()
        reloadCategories()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == assignmentsTableView {
            return 1
        } else {
            if course.categories.count > 0 {
                return course.categories.count
            }
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == assignmentsTableView && course.assignments.count > 0 {
            DataService.instance.selectedAssignment = course.assignments[indexPath.section]
            performSegue(withIdentifier: "AssignmentSegue", sender: self)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == assignmentsTableView {
            return 10
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == assignmentsTableView {
            let headerView = UIView()
            headerView.backgroundColor = UIColor.clear
            return headerView
        } else {
            return nil
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == assignmentsTableView {
            if course.assignments.count > 0 {return course.assignments.count}
            return course.assignments.count + 1
        } else {
            return 1
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == assignmentsTableView {
            if course.assignments.count == 0 {
                let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
                cell.textLabel?.text = "No Assignments"
                cell.textLabel?.font = UIFont(name: "Avenir Next", size: 25)
                cell.selectionStyle = .none
                return cell
            }
            if let cell = tableView.dequeueReusableCell(withIdentifier: "assignmentCell") as? AssignmentCell{
                cell.assignment = course.assignments[indexPath.section]
                cell.updateViews()
                if course.assignments[indexPath.section].category == course.unassignedCategory {
                    cell.assignmentNameLabel.textColor = #colorLiteral(red: 1, green: 0, blue: 0.05674913194, alpha: 1)
                    cell.assignmentCategoryLabel.textColor = #colorLiteral(red: 1, green: 0, blue: 0.05674913194, alpha: 1)
                } else {
                    cell.assignmentNameLabel.textColor = UIColor.black
                    cell.assignmentCategoryLabel.textColor = UIColor.black
                }
                return cell
            } else {
                return AssignmentCell()
            }
        } else {
            if course.categories.count == 0 {
                let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
                cell.textLabel?.text = "No Categories"
                return cell
            }
            if let cell = tableView.dequeueReusableCell(withIdentifier: "categoryCell") as? CategoryCell{
                if indexPath.row < course.categories.count {
                    cell.category = course.categories[indexPath.row]
                    cell.protocolDelegate = self
                    cell.updateView()
                    return cell
                } else {
                    return CategoryCell()
                }
            } else {
                return CategoryCell()
            }
        }
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    @IBAction func createCategoryAction(_ sender: Any) {
        categoryCreatePopover.modalPresentationStyle = UIModalPresentationStyle.popover
        categoryCreatePopover.popoverPresentationController?.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        categoryCreatePopover.popoverPresentationController?.delegate = self
        categoryCreatePopover.popoverPresentationController?.sourceView = view
        categoryCreatePopover.popoverPresentationController?.sourceRect = view.bounds
        categoryCreatePopover.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
        categoryCreatePopover.protocolDelegate = self
        categoryCreatePopover.doneProtocolDelete = self
        categoryCreatePopover.preferredContentSize = CGSize(width: 250, height: 150)
        UIView.animate(withDuration: 0.6) {
            self.blurView.alpha = 1.0
        }
        self.present(categoryCreatePopover, animated: true, completion: nil)
    }
    
    func update() {
        reloadCategories()
    }
    
    func sortTable(sortBy: String) {
        if sortBy == "Category" {
            sortedBy = "Category"
            course.assignments.sort(by: { (a1, a2) -> Bool in
                return a1.category.name < a2.category.name
            })
        } else if sortBy == "Name" {
            sortedBy = "Name"
            course.assignments.sort(by: { (a1, a2) -> Bool in
                return a1.name < a2.name
            })
        }
        assignmentsTableView.reloadData()
    }
    
    func reloadCategories(){
        course.categories.sort { (c1, c2) -> Bool in
            return c1.name < c2.name
        }
        categoryTableView.reloadData()
        var totalWeight:Double = 0
        for c in course.categories {
            totalWeight += c.weight
        }
        totalWeightLabel.text = String(format: "%.2f", totalWeight)
    }
    
    func done() {
        UIView.animate(withDuration: 0.6) {
            self.blurView.alpha = 0.0
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let createAssignmentVC = segue.destination as? CreateAssignmentVC {
            createAssignmentVC.course = self.course
        } else if let assignmentVC = segue.destination as? AssignmentVC {
            let backButton = UIBarButtonItem()
            backButton.title = "Back"
            assignmentVC.navigationItem.backBarButtonItem = backButton
        }
    }

}
