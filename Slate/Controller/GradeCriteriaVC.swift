//
//  GradeCriteriaVC.swift
//  Slate
//
//  Created by Eric Scaramuzzo on 12/8/17.
//  Copyright © 2017 Eric Scaramuzzo. All rights reserved.
//

import UIKit

protocol gradeCriteriaProtocol {
    func done()
}

class GradeCriteriaVC: UIViewController, UITableViewDataSource, UITableViewDelegate, updateCriteria {
    
    
    @IBOutlet var criteriaTableView: UITableView!
    
    var protocolDelegate:gradeCriteriaProtocol!
    var criterias = DataService.instance.selectedCourse!.gradeCriterias
    var oldHeight:CGFloat!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        criteriaTableView.delegate = self
        criteriaTableView.dataSource = self
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: NSNotification.Name.UIKeyboardWillShow,
            object: nil
        )
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 13
    }
    
    
    @objc func keyboardWillHide(){
        self.preferredContentSize = CGSize(width: view.bounds.width, height: oldHeight)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        self.preferredContentSize = CGSize(width: view.bounds.width, height: view.bounds.height)
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? GradeCriteriaCell {
            cell.gradeCriteria = criterias[indexPath.row]
            cell.updateView()
            cell.criteriaDelegate = self
            return cell
        } else {
            return GradeCriteriaCell()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        protocolDelegate.done()
    }
    
    func updateCriteria(gradeCriteria: GradeCriteria) {
        let index = criterias.index(of: gradeCriteria)!
        if index >= 1 {
            if criterias[index].upperBound - criterias[index - 1].lowerBound > 0.00 || (criterias[index].upperBound - criterias[index - 1].lowerBound > -0.01){
                let difference = criterias[index - 1].lowerBound - criterias[index].upperBound - 0.01
                criterias[index - 1].lowerBound? -= difference
            } else if criterias[index-1].lowerBound - criterias[index].upperBound > 0.01 {
                let difference = criterias[index - 1].lowerBound - criterias[index].upperBound - 0.01
                criterias[index - 1].lowerBound? -= difference
            }
        }
        criteriaTableView.reloadData()
    }
    
    @IBAction func doneButtonAction(_ sender: Any) {
        for c in criterias {
            if Double(String(format: "%.2f", c.lowerBound))! >= Double(String(format: "%.2f", c.upperBound))!  {
                let alert = UIAlertController(title: nil, message: "Error in bound \(c.name!)", preferredStyle: .alert)
                self.present(alert, animated: true, completion: nil)
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
                    alert.dismiss(animated: true, completion: nil)
                }
                return
            }
        }
        DataService.instance.selectedCourse?.criteriaHasBeenChanged = true
        DataService.instance.changesMade = true
        DispatchQueue.global(qos: .userInitiated).async {
            DataService.instance.save()
        }
        dismiss(animated: true, completion: nil)
    }
    

}
