//
//  AttendanceVC.swift
//  Slate
//
//  Created by Eric Scaramuzzo on 12/15/17.
//  Copyright © 2017 Eric Scaramuzzo. All rights reserved.
//

import UIKit

class AttendanceVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var attendanceLabel: UILabel!
    @IBOutlet var dateTakenLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var takeAttendanceTableView: UITableView!
    @IBOutlet var attendanceTableView: UITableView!
    let course = DataService.instance.selectedCourse!
    var students = DataService.instance.selectedCourse!.students
    var attendance : [String:Bool] = [:]
    
    var date:Date!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        takeAttendanceTableView.delegate = self
        takeAttendanceTableView.dataSource = self
        
        attendanceTableView.delegate = self
        attendanceTableView.dataSource = self
        
        takeAttendanceTableView.alpha = 0.0
        timeLabel.alpha = 0.0
        
        students = students.sorted(by: { (s1, s2) -> Bool in
            return s1.name < s2.name
        })
        
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Take Attendance", style: .plain, target: self, action: #selector(AttendanceVC.takeAttendance))
    }
    
    @objc func takeAttendance() {
        navigationItem.hidesBackButton = true
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(AttendanceVC.cancelTakingAttendance))
        UIView.animate(withDuration: 0.5) {
            self.takeAttendanceTableView.alpha = 1.0
            self.timeLabel.alpha = 1.0
            self.attendanceTableView.alpha = 0.0
            self.attendanceLabel.alpha = 0.0
            self.dateTakenLabel.alpha = 0.0
        }
        date = Date()
        
        for s in students {
            attendance[s.id] = false
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM dd,yyyy h:mm a"
        let myString = formatter.string(from: date)
        timeLabel.text = myString
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(AttendanceVC.doneTakingAttendance))
        takeAttendanceTableView.reloadData()
    }
    
    func editAttendance() {
        navigationItem.hidesBackButton = true
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(AttendanceVC.cancelTakingAttendance))
        takeAttendanceTableView.reloadData()
        UIView.animate(withDuration: 0.5) {
            self.takeAttendanceTableView.alpha = 1.0
            self.timeLabel.alpha = 1.0
            self.attendanceTableView.alpha = 0.0
            self.attendanceLabel.alpha = 0.0
            self.dateTakenLabel.alpha = 0.0
        }
    
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM dd,yyyy h:mm a"
        let myString = formatter.string(from: date)
        timeLabel.text = myString
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done Editing", style: .done, target: self, action: #selector(AttendanceVC.doneEditingAttendance))
    }
    
    @objc func doneEditingAttendance() {
        navigationItem.hidesBackButton = false
        navigationItem.leftBarButtonItem = nil
        UIView.animate(withDuration: 0.5) {
            self.takeAttendanceTableView.alpha = 0.0
            self.timeLabel.alpha = 0.0
            self.attendanceTableView.alpha = 1.0
            self.attendanceLabel.alpha = 1.0
            self.dateTakenLabel.alpha = 1.0
        }
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Take Attendance", style: .plain, target: self, action: #selector(AttendanceVC.takeAttendance))
        let newAttendance = Attendance()
        newAttendance.dateTaken = date
        newAttendance.attendance = attendance
        let index = course.attendances.index(of: newAttendance)
        course.attendances.remove(at: index!)
        course.attendances.append(newAttendance)
        course.attendances.sort { (a1, a2) -> Bool in
            return a1.dateTaken < a2.dateTaken
        }
        attendanceTableView.reloadData()
        self.takeAttendanceTableView.reloadData()
        DataService.instance.changesMade = true
        DispatchQueue.global(qos: .userInitiated).async {
            DataService.instance.save()
        }
    }
    
    @objc func doneTakingAttendance() {
        navigationItem.hidesBackButton = false
        navigationItem.leftBarButtonItem = nil
        UIView.animate(withDuration: 0.5) {
            self.takeAttendanceTableView.alpha = 0.0
            self.timeLabel.alpha = 0.0
            self.attendanceTableView.alpha = 1.0
            self.attendanceLabel.alpha = 1.0
            self.dateTakenLabel.alpha = 1.0
        }
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Take Attendance", style: .plain, target: self, action: #selector(AttendanceVC.takeAttendance))
        let newAttendance = Attendance()
        newAttendance.dateTaken = date
        newAttendance.attendance = attendance
        course.attendances.append(newAttendance)
        course.attendances.sort { (a1, a2) -> Bool in
            return a1.dateTaken < a2.dateTaken
        }
        attendanceTableView.reloadData()
        self.takeAttendanceTableView.reloadData()
        DataService.instance.changesMade = true
        DispatchQueue.global(qos: .userInitiated).async {
            DataService.instance.save()
        }
    }
    
    @objc func cancelTakingAttendance() {
        UIView.animate(withDuration: 0.5) {
            self.takeAttendanceTableView.alpha = 0.0
            self.timeLabel.alpha = 0.0
            self.attendanceTableView.alpha = 1.0
            self.attendanceLabel.alpha = 1.0
            self.dateTakenLabel.alpha = 1.0
        }
        navigationItem.hidesBackButton = false
        navigationItem.leftBarButtonItem = nil
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Take Attendance", style: .plain, target: self, action: #selector(AttendanceVC.takeAttendance))
        attendance = [:]
        self.takeAttendanceTableView.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if tableView == attendanceTableView {
            if editingStyle == .delete {
                confirmDeleteAttendance(attendance: course.attendances[indexPath.row])
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == attendanceTableView {
            return 50.0
        } else {
            return tableView.rowHeight
        }
    }
    
    func confirmDeleteAttendance(attendance: Attendance) {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM dd, yyyy h:mm a"
        let dateText = formatter.string(from: attendance.dateTaken)
        
        let alertView = UIAlertController(title: "Delete Attendance?", message: dateText, preferredStyle: .alert)
        
        let deleteAction = UIAlertAction(title: "Delete", style: .destructive) { (action) in
            self.deleteAttendance(attendance: attendance)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        
        alertView.addAction(deleteAction)
        alertView.addAction(cancelAction)
        
        present(alertView, animated: true, completion: nil)
    }
    
    func deleteAttendance(attendance: Attendance) {
        let index = course.attendances.index(of: attendance)
        course.attendances.remove(at: index!)
        course.attendances.sort { (a1, a2) -> Bool in
            return a1.dateTaken < a2.dateTaken
        }
        attendanceTableView.reloadData()
        DataService.instance.changesMade = true
        DispatchQueue.global(qos: .userInitiated).async {
            DataService.instance.save()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == takeAttendanceTableView {
            return students.count
        } else {
            return course.attendances.count
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == takeAttendanceTableView {
            let cell = tableView.cellForRow(at: indexPath)
            if cell?.accessoryType == .checkmark {
                attendance[students[indexPath.row].id] = false
                cell?.accessoryType = .none
            } else {
                attendance[students[indexPath.row].id] = true
                cell?.accessoryType = .checkmark
            }
        } else {
            attendance = course.attendances[indexPath.row].attendance
            date = course.attendances[indexPath.row].dateTaken
            editAttendance()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == takeAttendanceTableView {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "cell") {
                cell.textLabel?.text = students[indexPath.row].name
                cell.detailTextLabel?.text = "Id: " + students[indexPath.row].id
                cell.textLabel?.font = UIFont(name: "Avenir Next", size: 20)
                cell.selectionStyle = .none
                if cell.accessoryType == .checkmark {
                    cell.accessoryType = .none
                }
                if attendance.keys.count > 0 {
                    if attendance[students[indexPath.row].id] == true {
                        cell.accessoryType = .checkmark
                    }
                }
                return cell
            } else {
                return UITableViewCell()
            }
        } else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "attendanceCell") {
                let attendance = course.attendances[indexPath.row]
                let formatter = DateFormatter()
                formatter.dateFormat = "MMMM dd, yyyy h:mm a"
                cell.textLabel?.text = formatter.string(from: attendance.dateTaken)
                cell.textLabel?.font = UIFont(name: "Avenir Next", size: 18)
                var total = 0
                var totalPresent = 0
                for key in attendance.attendance.keys {
                    total += 1
                    if attendance.attendance[key] == true {
                        totalPresent += 1
                    }
                }
                cell.detailTextLabel?.text = "\(totalPresent)/\(total)"
                return cell
            } else {
                return UITableViewCell()
            }
        }
    }
    
    

}
